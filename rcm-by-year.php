<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>RCM by Year | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

            <!-- Single Post Start-->
            <div style="padding-top:0px;"class="single">
                <div class="about wow fadeInUp" data-wow-delay="0.1s">
                    <div class="container">
                        <div class="row">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#one"><b>&bull; 2015 - 2016</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#two"><b>&bull; 2016 - 2017</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#three"><b>&bull; 2017 - 2018</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#four"><b>&bull; 2018 - 2019</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#five"><b>&bull; 2019 - 2020</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#six"><b>&bull; 2020 - 2021</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#seven"><b>&bull; 2021 - 2022</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#eight"><b>&bull; 2022 - 2023</b></a>
                                </li>
                            </ul>
                        </div>
                        <!-- Tab Content -->
                        <div class="row">
                            <div class="tab-content">
                                <div id="one" class="tab-pane fade show active">
                                    <div class="row">
                                        <h3 class="mt-5">AVP RY 2015-2016</h3>
                                        <p></p>
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <video class="embed-responsive-item" controls>
                                                <source src="img/rcm-by-year/2015-2016/AVP RY 2015-2016.mp4" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </div>
                                <div id="two" class="tab-pane fade">
                                    <h3 class="mt-5">Rotary Journey of DG George Bunuan</h3>
                                    <h4>(Pictures taken from the 67th Anniversary Souvenir Program of RC Malolos)</h4>
                                    <!-- First row of images -->
                                    <div class="row about wow fadeInUp" data-wow-delay="0.1s">
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page0.jpg" alt="PSD File 1" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page1.jpg" alt="PSD File 2" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page2.jpg" alt="PSD File 3" width="100%">
                                        </div>
                                    </div>
                                    <!-- Second row of images -->
                                    <div class="row about wow fadeInUp" data-wow-delay="0.1s">
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page3.jpg" alt="PSD File 3" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page5.jpg" alt="PSD File 5" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page6.jpg" alt="PSD File 6" width="100%">
                                        </div>
                                    </div>
                                    <!-- Third row of images -->
                                    <div class="row about wow fadeInUp" data-wow-delay="0.1s">
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page6b.jpg" alt="PSD File 6b" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page7.jpg" alt="PSD File 7" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page8.jpg" alt="PSD File 8" width="100%">
                                        </div>
                                    </div>
                                    <!-- Fourth row of images -->
                                    <div class="row about wow fadeInUp" data-wow-delay="0.1s">
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page9.jpg" alt="PSD File 9" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page11.jpg" alt="PSD File 11" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page12.jpg" alt="PSD File 12" width="100%">
                                        </div>
                                    </div>
                                    <!-- Fifth row of images -->
                                    <div class="row about wow fadeInUp" data-wow-delay="0.1s">
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page13.jpg" alt="PSD File 13" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page14.jpg" alt="PSD File 14" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page15.jpg" alt="PSD File 15" width="100%">
                                        </div>
                                    </div>
                                    <!-- Sixth row of images -->
                                    <div class="row about wow fadeInUp" data-wow-delay="0.1s">
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page16.jpg" alt="PSD File 16" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        <img src="img/rcm-by-year/2016-2017/page17.jpg" alt="PSD File 17" width="100%">
                                        </div>
                                        <div class="col-md-4">
                                        
                                        </div>
                                    </div>
                                    <h3 class="mt-5">AVP Term of PDG George</h3>
                                    <div class="embed-responsive embed-responsive-16by9 about wow fadeInUp" data-wow-delay="0.1s">
                                        <video class="embed-responsive-item" controls>
                                            <source src="img/rcm-by-year/2016-2017/AVP Term of PDG George RY 2016-2017.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                    <hr>
                                    <h3 class="mt-3">AVP RY 2016-2017</h3>
                                    <div class="embed-responsive embed-responsive-16by9 about wow fadeInUp" data-wow-delay="0.1s">
                                        <video class="embed-responsive-item" controls>
                                            <source src="img/rcm-by-year/2016-2017/AVP RY 2016-2017.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                                <div id="three" class="tab-pane fade">
                                    <h3 class="mt-5">2017 - 2018</h3>
                                    <p>2017 - 2018 content.</p>
                                </div>
                                <div id="four" class="tab-pane fade">
                                    <h3 class="mt-5">2018 - 2019</h3>
                                    <p>2018 - 2019 content.</p>
                                </div>
                                <div id="five" class="tab-pane fade">
                                    <h3 class="mt-5">2019 - 2020</h3>
                                    <p>2019 - 2020 content.</p>
                                </div>
                                <div id="six" class="tab-pane fade">
                                    <h3 class="mt-5">2020 - 2021</h3>
                                    <p>2020 - 2021 content.</p>
                                </div>
                                <div id="seven" class="tab-pane fade">
                                    <h3 class="mt-5">Feed One Child Everyday</h3>
                                                <!-- Single Post Start-->
                                    <div class="video wow fadeIn" data-wow-delay="0.1s">
                                        <div class="container">
                                            <button type="button" class="btn-play" data-toggle="modal" data-src="img/real/Rotary Video Final.mp4" data-target="#videoModal">
                                                <span></span>
                                            </button>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-body">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <!-- 16:9 aspect ratio -->
                                                        <div class="embed-responsive embed-responsive-16by9">
                                                            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <?php
                                            renderImage(5, 'feed-one', 'feed');
                                            ?>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div id="eight" class="tab-pane fade">
                                    <h3 class="mt-5">2022 - 2023</h3>
                                    <p>2022 - 2023 content.</p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
              
            </div>
            <!-- Single Post End-->
            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
            </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "RCM by Year";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>

    </body>
</html>
