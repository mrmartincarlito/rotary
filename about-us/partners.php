<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
      <title>Partners | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/rcmlogo.png" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

            <!-- About Start -->
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="section-header text-left">
                                <h4>Partners</h4>
                            </div>
                            <div class="about-text">     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>
        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Partners";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
