<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Officers for RY | Rotary Club of Malolos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Construction Company Website Template" name="keywords">
    <meta content="Construction Company Website Template" name="description">
    <!-- Favicon -->
    <link href="img/rcmlogo.png" rel="icon">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- CSS Libraries -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/css-libraries.php') ?>
    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <!-- Nav Bar Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/top-menu.php') ?>
        <!-- Nav Bar End -->

        <!-- Page Header Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
        <!-- Page Header End -->

        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row justify-content-center">
                    <center>
                        <h3>RY 2023-2024</h3>
                        <hr />
                        <h4>Board of Directors</h4>
                    </center>
                    <hr />
                    <div class="col-md-3">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/danny.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4>President</h4>
                                <p>Pres. Danny Agustin</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/manny.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4>Vice President</h4>
                                <p>PP Manny Viardo</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/francis.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4>Secretary</h4>
                                <p>PN Francis De Guzman</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/tbd.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4>Treasurer</h4>
                                <p>Treas. Imelda Silang</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>
                    <hr />
                    <center>
                    <h4>Directors</h4>
                    </center>
                    <hr />
                    <div class="col-md-2">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/coco.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4></h4>
                                <p>PE Coco Bunuan</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/setti.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4></h4>
                                <p>IPP Setti Tanwangco</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/tbd.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4></h4>
                                <p>Dir Ron Natividad</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/maritess.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4></h4>
                                <p>Dir Marites Gan</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/joseph.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4></h4>
                                <p>Dir Joseph Cruz</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="list-item">
                            <center>
                            <img class="member-pic" src="../img/members/tbd.jpg" alt="Image 1">
                            <div class="content">
                            
                                <h4></h4>
                                <p>Dir Robert Santillan</p>
                            
                            </div>
                            </center>
                        </div>
                    </div>
                    <hr />

                    
                    <div class="col-lg-4 col-md-6">
                        <ul style="list-style-type:none;">
                            <li><strong>Auditor</strong></li>
                            <li><strong>Sgt at Arms</strong></li>
                            <li><strong>President – Elect (RY 2024-2025)</strong></li>
                            <li><strong>President Nominee (RY 2025-2026)</strong></li>
                            <li><strong>Club Trainor</strong></li>
                            <li><strong>Committee Chairmen</strong></li>
                            <li style="margin-left:40px;">Administration</li>
                            <li style="margin-left:40px;">Membership</li>
                            <li style="margin-left:40px;">Service Projects</li>
                            <li style="margin-left:40px;">TRF</li>
                            <li style="margin-left:40px;">Public Image</li>
                            <li style="margin-left:40px;">Youth Service</li>
                            <br>
                            <li><strong>Advisers</strong></li>
                            <br><br><br>
                            
                            <br>
                            <li><strong>Executive Secretary</strong></li>


                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <ul style="list-style-type:none;">
                            
                            <li>PP Romy Lazaro</li>
                            <li>Rtn Glenn Uyengco</li>
                            <li>PE Coco Bunuan</li>
                            <li>PN Francis De Guzman</li>
                            <li>IPP Setti Tanwangco</li>
                            <br>
                            <li>Dir Marites Gen</li>
                            <li>PP Philip Cruz</li>
                            <li>Dir Robert Santillan</li>
                            <li>PP Ed Bernal</li>
                            <li>IPP Setti Tanwangco</li>
                            <li>Rtn Rachel Crisostomo</li>
                            <br>
                            <li>PDG Boy Aniag</li>
                            <li>PDG George Bunua</li>
                            <li>PP Benjie De Mesa</li>
                            <li>PP Rod Sumulong</li>
                            <br>
                            <li>Mr. Ranie Bernabe</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- About End -->
        <!-- Footer Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/footer.php') ?>
        <!-- Footer End -->

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <!-- JavaScript Libraries -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/js.php') ?>
    <!-- Template Javascript -->
    <script src="js/main.js"></script>
    <script>
            var dynamicHeading = "Officers for RY";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
</body>

</html>