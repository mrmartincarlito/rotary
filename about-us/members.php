<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Our Members | Rotary Club of Malolos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Construction Company Website Template" name="keywords">
    <meta content="Construction Company Website Template" name="description">
    <!-- Favicon -->
    <link href="img/rcmlogo.png" rel="icon">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- CSS Libraries -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/css-libraries.php') ?>
    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .middle {
            vertical-align: middle !important;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <!-- Nav Bar Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/top-menu.php') ?>
        <!-- Nav Bar End -->

        <!-- Page Header Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
        <!-- Page Header End -->

        <!-- Team Start -->
        <div class="row justify-content-center">
            <div class="col-auto">
                <table class="table table-striped table-responsive">
                    <thead class="thead-dark">
                        <tr>
                            <th>NAME</th>
                            <!-- <th>PP RY</th> -->
                            <th>CLASSIFICATION</th>
                            <th>CURRENT POSITION</th>
                            <!-- <th>DATE JOINED</th>
                            <th>BIRTHDATE</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="../img/members/danny.jpg" class="member-pic"><p>AGUSTIN, DANNY</p></td>
                            <!--  <td class="middle">RY 2023-2024</td> --> 
                            <td class="middle">Catering Services</td>
                            <td class="middle">President</td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/boy.jpg" class="member-pic"><p>ANIAG, BOY</p></td>
                            
                            <!--  <td class="middle">RY 2023-2024</td> --> 
                            <td class="middle">Commercial Printing</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>AQUINO, MICHAEL</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Local Government Services</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>

                        <tr>
                            <td><img src="../img/members/danny.jpg" class="member-pic"><p>BAUTISTA, MIGUEL</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Entrepreneur</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/ed.jpg" class="member-pic"><p>BERNAL, ED</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Renewable Energy Mgt.</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/danny.jpg" class="member-pic"><p>BORLONGAN, DANNY</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Fish Broker</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/coco.jpg" class="member-pic"><p>BUNUAN, COCO</p></td>
                        
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Computer Programming</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/george.jpg" class="member-pic"><p>BUNUAN, GEORGE</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Human Resource Mgt.</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/lanie.jpg" class="member-pic"><p>CAPULE, MAYLANIE</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Travel Agency</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>CRISOSTOMO, RACHEL</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Kennel/Dog Breeder</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>CRUZ, FELIPE</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Avionics Instrumentation</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/joseph.jpg" class="member-pic"><p>CRUZ, JOSEPH</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Garments and Textile Printing</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tirso.jpg" class="member-pic"><p>CRUZ, TIRSO</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">IT & Software Development</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>

                        <tr>
                            <td><img src="../img/members/bhey.jpg" class="member-pic"><p>CUNANAN, BHEY</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Trading / Health and Wellness</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/francis.jpg" class="member-pic"><p>DE GUZMAN, FRANCIS</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Commercial Printing</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>DE MESA, BENJIE</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Medicine Pediatrics</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/andy.jpg" class="member-pic"><p>DEL ROSARIO, ANDY</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Bakery Business</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/john.jpg" class="member-pic"><p>FONTILLAS, JOHN</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Tire Services</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/maritess.jpg" class="member-pic"><p>GAN, MARITES</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Logistics</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/arnold.jpg" class="member-pic"><p>HERNANDEZ, ARNOLD</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Petroleum Prod. Distribution</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/ken.jpg" class="member-pic">p <p>LAQUINDANUM, KEN</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Building Construction</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>LAVINA, GAEL</p></td>
                        
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Medicine</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/romy.jpg" class="member-pic"><p>LAZARO, ROMY</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Land Surveying</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/ronald.jpg" class="member-pic"><p>NATIVIDAD, RONALD</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Insurance(non-life)</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>ROXAS, RODRIGO</p></td>
                        
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Public School Mgt</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/rome.jpg" class="member-pic"><p>SANTIAGO, ROME</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Local Government Services</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>SANTILLAN, ROBERT</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Chief Marine Engineer / Entrepreneur</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>SILANG, IMELDA</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Door to Door Services</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/rod.jpg" class="member-pic"><p>SUMULONG, ROD</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Optometry</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/setti.jpg" class="member-pic"><p>TANWANGCO, SETTI</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Hotel & Restaurant Management</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/tbd.jpg" class="member-pic"><p>UYENGCO, GLENN</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Kennel/ Dog Breeder</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        <tr>
                            <td><img src="../img/members/manny.jpg" class="member-pic"><p>VIARDO, MANNY</p></td>
                            
                            <!-- <td class="middle">RY 2023-2024</td> -->
                            <td class="middle">Apartment Leasing</td>
                            <td class="middle"></td>
                            <!-- <td class="middle"></td>
                            <td class="middle"></td> -->
                        </tr>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Footer Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/footer.php') ?>
        <!-- Footer End -->
        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>
    <!-- JavaScript Libraries -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/js.php') ?>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
    <script>
            var dynamicHeading = "Members";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
</body>

</html>