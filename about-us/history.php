<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>History | Rotary Club of Malolos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Construction Company Website Template" name="keywords">
    <meta content="Construction Company Website Template" name="description">
    <!-- Favicon -->
    <link href="img/rcmlogo.png" rel="icon">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <!-- CSS Libraries -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/css-libraries.php') ?>
    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <!-- Nav Bar Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/top-menu.php') ?>
        <!-- Nav Bar End -->

        <!-- Page Header Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
        <!-- Page Header End -->

        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-12 col-md-12">
                        <div class="section-header text-left">
                            <h4> History of the Rotary Club of Malolos</h4>
                        </div>
                        <div class="about-text justify-content-end">
                            <p>The Rotary Club of Malolos was the 15th Rotary club organized in the Philippines. But its organization was unique in the sense Rotary was not brought to Malolos but was asked for. </p>

                            <p>Francisco “Frank” Aniag, then municipal secretary of Malolos, in October 1948, conceived the idea of organizing a Rotary club in the province of Bulacan. He broached the idea to then Provincial Treasurer Vicente “Titong” Avila, then Provincial Fiscal Feliciano “Fely” Torres, and Teofilo “Magmamani” Sauco. Together they invited prominent citizens from different municipalities of Bulacan to explain what they knew about Rotary from their readings and the need to organize a Rotary club in Bulacan.</p>

                            <p>For their efforts, PP Frank Aniag was conferred the title of “The Pioneer Rotarian of Bulacan” and the four were dubbed as the first Rotary converts of Bulacan.</p>

                            <p>The group, originally consisting of 28, was already meeting regularly at the Panciteria Canton in Malolos when PP Frank contacted Rotary authorities in Manila, notably PDG Benjamin Gaston, PDG Emilio Javier and PDG Eduardo Romualdez, who was then President of the Rotary Club of Manila, for the possibility of sponsoring Rotary Club of Malolos. Thereafter, a delegation of Manila Rotarians came to finalize its sponsoring of the new club.</p>

                            <p>The club’s charter was granted by Rotary International on July 25, 1949 with Teofilo Reyes Sr. as charter president and Frank Aniag as charter secretary. </p>

                            <p>In November 1949, the Inner Wheel Club of Malolos, consisted of the spouses of the members of Rotary Club of Malolos was also organized and to this day, remains to be one of the active clubs in Inner Wheel Club of the Philippines, Inc. District 377.</p>

                            <p>In 1953, RC Malolos sponsored the Rotary Club of Puerto Princesa through the efforts of Rtn. Titong Avila. Through the initiative of PP Maxie Valenzuela, RC Malolos started to expand Rotary to outlying cities and municipalities. It organized the Rotary Club of Caloocan in 1959 and another 11 clubs in Bulacan, they are the following:</p>

                            <div class="container ">
                                <div class="row justify-content-center">
                                    <div class="mx-auto" style="width: 200px;">
                                        <p>1971 - RC Baliwag</p>
                                        <p>1974 - RC Valenzuela</p>
                                        <p>1974 - RC Meycauayan</p>
                                        <p>1976 - RC Sta. Maria</p>
                                        <p>1980 - RC Hagonoy</p>
                                        <p>1980 - RC Bocaue</p>
                                    </div>
                                    <div class="mx-auto" style="width: 200px;">
                                        <p>1981 - RC Plaridel</p>
                                        <p>1982 - RC Balagtas</p>
                                        <p>1984 - RC Bulakan</p>
                                        <p>1994 - RC Barasoain</p>
                                        <p>2000 - RC Malolos Hiyas</p>
                                    </div>
                                </div>
                            </div>
                            <p>RC Malolos produced four (4) district governors, namely:</p>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="mx-auto" style="width: 600px;">
                                        <p>1970-1971 PDG Sabino “ Benny” Santos for D-380 , he later became RI Director in 1979, served in the RI Board in 1989-1991, and also became a chair of Philippine College of Rotary Governors (PCRG)</p>
                                        <p>1987-1988 PDG Francisco “Jun” Aniag, Jr., the first governor of District 3770, he later served also as chair of PCRG</p>
                                        <p>1992-1993 PDG Pacifico “Boy” Aniag</p>
                                        <p>2016-2017 PDG George V. Bunuan</p>
                                    </div>
                                </div>
                            </div>
                            <p>However, there were other three (3) former members of RC Malolos who transferred to other clubs and later became district governors also, they were:</p>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="mx-auto" style="width: 600px;">
                                        <p>1984-1985 PDG Fil Mendoza, transferred to RC Sta. Maria, D3770</p>
                                        <p>1997-1998 PDG Manny Punzalan, transferred to RC Calumpit, D3770</p>
                                        <p>2016-2017 PDG Raul Peralta, transferred to RC Freeport Zone Subic Bay, D3790</p>
                                    </div>
                                </div>
                            </div>
                            <p>In 1978, when RI was looking for a worldwide project through its 3-H (Health, Hunger and Humanity) Committee, then PDG Benny Santos who remained an active member of the Rotary Club of Malolos wrote RI that if Rotary could provide the anti-polio vaccine, they would mobilize all the Rotarians in the entire Philippines and immunize all the children against polio virus. It was approved and budget was provided to immunize some 6 million children in a period of 5 years. It was a huge success hence RI decided in 1985 to implement Polio Plus globally with the target to completely eradicate polio in the face of the earth.</p>

                            <p>RC Malolos played a historic role in the polio eradication program of Rotary in the Philippines and contributed to the success of the “greatest humanitarian program” of Rotary.</p>

                            <p>In October 2019 at the age of 96, Past RI President M. A. T. Caparas joined and was inducted as a member of the Rotary Club of Malolos, he was the only Filipino that became RI President. He actually grew up in Malolos so his joining of RC Malolos was actually a homecoming. He was a charter member of the Rotary Club of Caloocan that was sponsored by RC Malolos in 1959. PRIP Mat Caparas was among the RI leaders that launched the Rotary’s polio eradication program in the Philippines in 1979.</p>

                            <p>Until 2017, RC Malolos was an all male club. It was only in 2018 that the club started accepting female members through the initiative of PDG Jun Aniag. And in 2022, at the same time that Rotary International had its first woman president, the Rotary Club of Malolos also had its first woman president in the person of Rossette “Setti” Tanwangco. </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
        </div>

        <!-- Footer Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/footer.php') ?>
        <!-- Footer End -->

        <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/js.php') ?>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
    <script>
            var dynamicHeading = "History";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
</body>

</html>