<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Club Hymn | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
            <link href="img/rcmlogo.png" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
        <!-- Nav Bar Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
        <!-- Nav Bar End -->

        <!-- Page Header Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
        <!-- Page Header End -->

            <!-- About Start -->
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="section-header text-left">
                              <h4>Club Hymn</h4>
                              <audio controls>
                                <source src="img/members/audio.mp3" type="audio/ogg">
                                <source src="img/members/audio.mp3" type="audio/mpeg">
                                    Your browser does not support the audio element.
                              </audio>
                            </div>
                            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                                <div class="container">
                                    <div class="section-header text-center">
                                        <p>(Lyrics Ricardo “Leony” Liongson)<br/> (Music: PP Jose “JB”Robles)</p>
                                        <h2>RISE MALOLOS ROTARY</h2>
                                    </div>

                                    <div class="about-text text-center">
                                        <p>
                                            We’ve set our shoulders to the wheels<br/>
                                            Rotary aim to fulfill<br/>
                                            For men’s uplift strive hard we will<br/>
                                            And his lot we’ll champion still<br/><br/><br/>

                                            Our life’s work we’ll dignify<br/>
                                            With norms that are sublime and high<br/>
                                            Cave thou a name in the history<br/>
                                            Rise! Malolos Rotary<br/><br/><br/>

                                            Will spring off love, faith and good cheer<br/>
                                            In a world be set with fear<br/>
                                            Our voices ring out for all to hear<br/>
                                            Thy precepts we hold so clear<br/><br/><br/>

                                            Peace in our time our fondest dream<br/>
                                            That joy may flow in the endless stream<br/>
                                            Keep thou thy tryst with destiny <br/>
                                            Rise Malolos Rotary
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                                <div class="container">
                                        <div class="section-header text-center">
                                        <p>(Lyrics: PP Ricardo “Carding” Nicolas) </p>
                                        <h2>TINDIG MALOLOS ROTARY</h2>
                                    </div>
                                    <div class="about-text text-center">
                                    <p>
                                    Lahat ay naninindigan<br/>
                                    Rotary’y magtagumpay<br/>
                                    Kapwa tao at tulungan<br/>
                                    Paglingkuran ang tanan<br/><br/><br/>

                                    Mamuhay ng Marangal,<br/>
                                    Gawa’y tumpak, wasto’t banal<br/>
                                    Sila muna bago kami <br/>
                                    Tindig! Malolos Rotary. (2x)
                                    </p>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
            <!-- About End -->
        </div>
        
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Club Hymn";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
