<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Past Presidents | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
            <link href="img/rcmlogo.png" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
        <style>
        .image-caption {
            text-align: center;
            font-weight: bold;
            margin-top: 10px;
        }
    </style>
    </head>
    <body>
        <div class="wrapper">
        <!-- Nav Bar Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
        <!-- Nav Bar End -->
        
        <!-- Page Header Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
        <!-- Page Header End -->

            <!-- About Start -->
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <h3>Past Presidents</h3>

                    <div class="row">
                        <?php
                        // Path to the folder containing the images
                        $imageFolder = '../img/past-presidents/yearly/';

                        // Get an array of image filenames from the folder
                        $imageFiles = glob($imageFolder . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);

                        // Display the images
                        $counter = 0;
                        foreach ($imageFiles as $imageFile) {
                            ?>
                            <div class="col-md-4">
                                <div class="card mb-3">
                                    <img src="<?php echo $imageFile; ?>" alt="Image" class="card-img-top">
                                    <div class="card-body text-center">
                                        <h5 class="card-title"><?php echo pathinfo($imageFile, PATHINFO_FILENAME); ?></h5>
                                        <!-- Additional card content can be added here -->
                                    </div>
                                </div>
                            </div>
                            <?php
                            $counter++;
                            if ($counter % 3 === 0) {
                                echo '</div><div class="row">';
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- About End -->
        </div>
        
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Past Presidents";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
