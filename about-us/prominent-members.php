<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
      <title>Prominent Members | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/rcmlogo.png" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->
            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

            <!-- About Start -->
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                                <div class="container">
                                    <div class="row align-items-center">
                                        <div class="col-lg-5 col-md-6">
                                            <div class="about-img">
                                                <img src="img/past-presidents/mateo-caparas.jpg" alt="Image">
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-6">
                                            <div class="section-header text-left">
                                                <p>Past Rotary International President</p>
                                                <h2>Mateo A.T. “M.A.T.” Caparas</h2>
                                            </div>
                                            <div class="about-text">
                                               <p>Mateo A.T. Caparas, a past Rotary International president who championed Rotary’s earliest efforts to fight polio and created the program that became Rotary Community Corps, died 15 July, 2020. He was 96.</p>
                                               <p>Caparas, known as M.A.T., served as RI president in 1986-87 and used his term to encourage members to inspire hope, through service, in communities around the world.</p>
                                               <p>The only RI president from the Philippines, Caparas also served as vice president, director, Rotary Foundation trustee and chair, committee member and chair, International Assembly group discussion leader, and district governor. A Rotarian since 1959, he was a member of the Rotary Club of Malolos, Bulacan, Philippines.</p>
                                               <p>Caparas was an accomplished labor lawyer. In the Philippine government, he served as a delegate to the constitutional convention and as chair of a presidential commission tasked with recovering assets allegedly looted by the country's ousted president, Ferdinand Marcos. A graduate of the University of the Philippines and Harvard Law School, he also had been director of the Philippine Bar Association and a member of the State Bar of California, USA. He retired as a partner at the law firm Caparas and Ilagan.</p><br>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                            <div class="about-text">
                                                <br>
                                                <p>Known to many Rotary members for his mild-mannered and genteel personality, Caparas embodied his presidential theme, Rotary Brings Hope, by creating the Rotary Village Corps. This </p>
                                                <p>initiative allowed clubs to offer support to the projects and activities that people in developing countries were carrying out. The program later became Rotary Community Corps.</p>
                                                <p>Caparas was awarded The Rotary Foundation's Distinguished Service Award and its Citation for Meritorious Service in 2002-03. For his dedication and service to a polio-free world, he received the PolioPlus Pioneer Award. In 1979, at the launch of Rotary's first polio project, a vaccination campaign in the Philippines, Caparas spoke about his hope for such a world:</p>
                                                <p>"We are here to mark the beginning, a very small beginning, of what we hope will be a great thing — the total eradication from the face of the earth of a dreaded disease, poliomyelitis," he said. "Great things always have a small beginning. When Paul Harris founded Rotary almost 75 years ago, there were just four people, and yet that organization which he founded has now flourished to cover 153 countries. It is our hope that in this small beginning, we would have, as I said, the great thing — the eradication of one of the scourges of mankind."</p>
                                                <p>More recently, Caparas was a member of various corporate boards where he promoted Rotary programs. He was also involved in promoting universal literacy with the Megumi Reader, an electronic device that aimed to improve students' reading ability. He also previously had been director of a local Boy Scouts council and a trustee of the Caloocan chapter of the Philippine Red Cross.</p>
                                                <p>Caparas is survived by two sons, Jorge (Charissa) and Mateo (Chona), and one daughter, Pilar de Villa (Jovy), as well as eight grandchildren and two great-grandchildren.</p>
                                                <p>Rotary International<br>23-Jul-2020</p>
                                            </div>
                                        </div>
                                      

                                    </div>
                                </div>
                            </div> 
                    </div>
                </div>
            </div>
            <!-- About End -->
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>
        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Prominent Members";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
