<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
      <title>Roster of Members | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">
            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->
            <!-- Page Header Start -->
            <div class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2>About RCM</h2>
                            <h3>Roster of Members</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page Header End -->
            <!-- About Start -->
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row justify-content-center">
                        <table class="table table-striped">
                            <thead class="thead-dark">
                                <tr>
                                <th>No.</th>
                                <th>NAMES</th>
                                <th>CLASSIFICATION</th>
                                <th>SPOUSE</th>
                                <th>CONTACT NO.</th>
                                <th>EMAIL</th>
                                <th>MEMBER ID</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td>1</td>
                                <td>AGUSTIN, DANNY</td>
                                <td>Catering Services</td>
                                <td>Malu</td>
                                <td>9175935803</td>
                                <td>dnagustin6@gmail.com</td>
                                <td>10775719</td>
                                </tr>
                                <tr>
                                <td>2</td>
                                <td>ANIAG, BOY</td>
                                <td>Commercial Printing</td>
                                <td>Alice</td>
                                <td>9175161588</td>
                                <td>pbaniag@gmail.com</td>
                                <td>262304</td>
                                </tr>
                                <tr>
                                <td>3</td>
                                <td>AQUINO, MICHAEL</td>
                                <td>Local Government Service</td>
                                <td></td>
                                <td>9272028290</td>
                                <td></td>
                                <td>10299397</td>
                                </tr>

                                <tr>
                                <td>4</td>
                                <td>BAUTISTA, MIGUEL</td>
                                <td>Entrepreneur</td>
                                <td>EJ</td>
                                <td>9175810555</td>
                                <td>migueltengcobautista@yahoo.com</td>
                                <td>10299409</td>
                                </tr>
                                <tr>
                                <td>5</td>
                                <td>BERNAL, ED</td>
                                <td>Renewable Energy Mgt.</td>
                                <td>Jean</td>
                                <td>9178057368</td>
                                <td>edgbern12@gmail.com</td>
                                <td>7044764</td>
                                </tr>
                                <tr>
                                <td>6</td>
                                <td>BORLONGAN, DANNY</td>
                                <td>Fish Brokerage</td>
                                <td>Josie</td>
                                <td>9178009765</td>
                                <td>danilo.borlongan@gmail.com</td>
                                <td>2272329</td>
                                </tr>
                                <tr>
                                <td>7</td>
                                <td>BUNUAN, COCO</td>
                                <td>Computer Programming</td>
                                <td>George</td>
                                <td>9189082216</td>
                                <td>coco_0430@yahoo.com</td>
                                <td>6222564</td>
                                </tr>
                                <tr>
                                <td>8</td>
                                <td>BUNUAN, GEORGE</td>
                                <td>Human Resource Mgt.</td>
                                <td>Coco</td>
                                <td>9189082215</td>
                                <td>gvbunuan@gmail.com</td>
                                <td>10299431</td>
                                </tr>
                                <tr>
                                <td>9</td>
                                <td>CAPULE, MAYLANIE</td>
                                <td>Travel Agency</td>
                                <td>Ernie</td>
                                <td>9778040148</td>
                                <td>enricocapule@yahoo.com</td>
                                <td>10440936</td>
                                </tr>
                                <tr>
                                <td>10</td>
                                <td>CRISOSTOMO, RACHEL</td>
                                <td>Kennel/ Dog Breeder</td>
                                <td>Glenn</td>
                                <td>9287553736</td>
                                <td>grayrainkennel@gmail.com</td>
                                <td>11744898</td>
                                </tr>
                                <tr>
                                <td>11</td>
                                <td>CRUZ, FELIPE</td>
                                <td>Avionics Instrumentation</td>
                                <td>Malou</td>
                                <td>9229241065</td>
                                <td>aeromaritime1@hotmail.com</td>
                                <td>7044772</td>
                                </tr>
                                <tr>
                                <td>12</td>
                                <td>CRUZ, JOSEPH</td>
                                <td>Garments & Textile Printing</td>
                                <td>Joan</td>
                                <td>9175765261</td>
                                <td>cruzjoseph10@yahoo.com</td>
                                <td>10440927</td>
                                </tr>
                                <tr>
                                <td>13</td>
                                <td>CRUZ, TIRSO</td>
                                <td>IT Software Development</td>
                                <td>Vicky</td>
                                <td>9228635969</td>
                                <td>smartcom_solution@yahoo.com</td>
                                <td>8656817</td>
                                </tr>

                                <tr>
                                <td>14</td>
                                <td>CUNANAN, BHEY</td>
                                <td>Trading / Health and Wellness</td>
                                <td></td>
                                <td>9338172180</td>
                                <td>bhey0811cunanan@gmail.com</td>
                                <td>7044779</td>
                                </tr>
                                <tr>
                                <td>15</td>
                                <td>DE GUZMAN, FRANCIS</td>
                                <td>Commercial Printing</td>
                                <td></td>
                                <td>9178517849</td>
                                <td>thisisfransys@gmail.com</td>
                                <td>10299445</td>
                                </tr>
                                <tr>
                                <td>16</td>
                                <td>DE MESA, BENJIE</td>
                                <td>Medicine Pediatrics</td>
                                <td>Jenny</td>
                                <td>9285077262</td>
                                <td>demesa_benjie@yahoo.com</td>
                                <td>262316</td>
                                </tr>
                                <tr>
                                <td>17</td>
                                <td>DEL ROSARIO, ANDY</td>
                                <td>Bakery Business</td>
                                <td>Azon</td>
                                <td>9085646927</td>
                                <td>andresdelrosario@rocketmail.com</td>
                                <td>1815472</td>
                                </tr>
                                <tr>
                                <td>18</td>
                                <td>FONTILLAS, JOHN</td>
                                <td>Tire Services</td>
                                <td>TinTin</td>
                                <td>9228709633</td>
                                <td>johnpaul_fontillas@yahoo.com</td>
                                <td>6949749</td>
                                </tr>
                                <tr>
                                <td>19</td>
                                <td>GAN, MARITES</td>
                                <td>Logistics</td>
                                <td>Jimmy</td>
                                <td>9282764105</td>
                                <td>mbdgan@gmail.com</td>
                                <td>10775715</td>
                                </tr>
                                <tr>
                                <td>20</td>
                                <td>GRAIDA, ROSALIE</td>
                                <td>Local Government Service</td>
                                <td>Patrick</td>
                                <td>9258321530</td>
                                <td>sahlee.adriano@yahoo.com</td>
                                <td>10775722</td>
                                </tr>
                                <tr>
                                <td>21</td>
                                <td>HERNANDEZ, ARNOLD</td>
                                <td>Petroleum Prod. Distribution</td>
                                <td>Maili</td>
                                <td>9499926574</td>
                                <td>arnhernz@yahoo.com</td>
                                <td>622563</td>
                                </tr>
                                <tr>
                                <td>22</td>
                                <td>HUNT, PATRICK</td>
                                <td>Retired-US Army</td>
                                <td>Sally</td>
                                <td>9257583466</td>
                                <td>tank666rph@gmail.com</td>
                                <td>10775711</td>
                                </tr>
                                <tr>
                                <td>23</td>
                                <td>LAQUINDANUM, KEN</td>
                                <td>Building Construction</td>
                                <td></td>
                                <td>9256769068</td>
                                <td>kml32517@gmail.com</td>
                                <td>10299348</td>
                                </tr>

                                <tr>
                                <td>24</td>
                                <td>LAVINA, GAEL</td>
                                <td>Medicine</td>
                                <td>Lenny</td>
                                <td>9177910377</td>
                                <td>galjays@yahoo.com</td>
                                <td>11026753</td>
                                </tr>
                                <tr>
                                <td>25</td>
                                <td>LAZARO, ROMY</td>
                                <td>Land Surveying</td>
                                <td>Hermie</td>
                                <td>9173623945</td>
                                <td>romulolazaro@yahoo.com</td>
                                <td>10300283</td>
                                </tr>
                                <tr>
                                <td>26</td>
                                <td>NATIVIDAD, RONALD</td>
                                <td>Insurance(non-life)</td>
                                <td>Jennet</td>
                                <td>9178308946</td>
                                <td>ronald_natividad@cocogen.com</td>
                                <td>11744903</td>
                                </tr>
                                <tr>
                                <td>27</td>
                                <td>ROXAS, RODRIGO</td>
                                <td>Public School Mgt</td>
                                <td>Lisa</td>
                                <td>9663410947</td>
                                <td>joaquintoni0482@gmail.com</td>
                                <td>10440940</td>
                                </tr>
                                <tr>
                                <td>28</td>
                                <td>SANTIAGO, ROME</td>
                                <td>Local Government Services</td>
                                <td>Cecilia</td>
                                <td>9650639925</td>
                                <td></td>
                                <td>11024281</td>
                                </tr>

                                <tr>
                                <td>29</td>
                                <td>SANTILLAN, ROBERT</td>
                                <td>Chief Marine Engineer / Entrepreneur</td>
                                <td></td>
                                <td>9174245018</td>
                                <td>robertcs28@yahoo.com.ph</td>
                                <td>11744900</td>
                                </tr>
                                <tr>
                                <td>30</td>
                                <td>SILANG, IMELDA</td>
                                <td>Door to Door Services</td>
                                <td></td>
                                <td>9163122407</td>
                                <td>imeldasilang@yahoo.com.ph</td>
                                <td>10299451</td>
                                </tr>
                                <tr>
                                <td>31</td>
                                <td>SUMULONG, ROD</td>
                                <td>Optometry</td>
                                <td>Zeny</td>
                                <td>9175174789</td>
                                <td></td>
                                <td>262277</td>
                                </tr>
                                <tr>
                                <td>32</td>
                                <td>TANWANGCO, SETTI</td>
                                <td>Hotel &amp; Restaurant Management</td>
                                <td></td>
                                <td>9179510551</td>
                                <td>tanwangco.rossette@gmail.com</td>
                                <td>10299440</td>
                                </tr>
                                <tr>
                                <td>33</td>
                                <td>UYENGCO, GLENN</td>
                                <td>Kennel/ Dog Breeder</td>
                                <td>Rachel</td>
                                <td>9088980648</td>
                                <td>grayrainkennel@gmail.com</td>
                                <td>11674478</td>
                                </tr>

                                <tr>
                                <td>34</td>
                                <td>VIARDO, MANNY</td>
                                <td>Apartment Leasing</td>
                                <td>Helen</td>
                                <td>9284633575</td>
                                <td>nativehotcoco@yahoo.com</td>
                                <td>874534</td>
                                </tr>
                                <tr>
                                <td>35</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>
                                <tr>
                                <td>36</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>
                                <tr>
                                <td>37</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- About End -->
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>
        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>
</html>
