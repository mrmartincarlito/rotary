<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
      <title>About RCM | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">

        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>

  <body>
    <div class="wrapper">

      <!-- Nav Bar Start -->
      <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
      <!-- Nav Bar End -->
            
            
            <!-- Page Header Start -->
            <div class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2>About RCM</h2>
                        </div>
                        <div class="col-12">
                            <a href="">Home</a>
                          <a href="">About RC Malolos</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page Header End -->


            <!-- About Start -->
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="section-header text-left">
                                <p>Rotary Club Malolos</p>
                              <h2>The History</h2>
                              <audio controls>
                                <source src="img/members/audio.mp3" type="audio/ogg">
                                <source src="img/members/audio.mp3" type="audio/mpeg">
                              Your browser does not support the audio element.
                              </audio>
                            </div>
                            <div class="about-text">
                                <p>
                                  The Rotary Club of Malolos was the 15th Rotary club organized in the Philippines. But its organization was unique in the sense Rotary was not brought to Malolos but was asked for.
                                </p>
                                <p>
                                PP Francisco “Frank” Aniag, then municipal secretary of Malolos, in October 1948, conceived the idea of organizing a Rotary club in the province of Bulacan. He broached the idea to then Provincial Treasurer Vicente “Titong” Avila, then Provincial Fiscal Feliciano “Fely” Torres, and Teofilo “Magmamani” Sauco. Together they invited prominent citizens from different municipalities of Bulacan to explain what they knew about Rotary from their readings and the need to organize a Rotary club in Bulacan.
                                </p>
                                <p>
                                For their efforts, PP Frank Aniag was conferred the title of “The Pioneer Rotarian of Bulacan” and the four were dubbed as the first Rotary converts of Bulacan.
                                </p>
                                <p>
                                The group, originally consisting of 28, was already meeting regularly at the Panciteria Canton in Malolos when PP Frank contacted Rotary authorities in Manila, notably PDG Benjamin Gaston, PDG Emilio Javier and PDG Eduardo Romualdez, who was then President of the Rotary Club of Manila, for the possibility of sponsoring it to Rotary. Thereafter, a delegation of Manila Rotarians came to finalize its sponsoring of the new club.
                                </p>
                                <p>
                                The club’s charter was granted by Rotary International on July 25, 1949 with Teofilo Reyes Sr. as charter president and Frank Aniag as charter secretary.
                                </p>
                                <p>
                                In November 1949, the Inner Wheel Club of Malolos, consisted of the spouses of the members of the Rotary Club of Malolos was also organized and to this day, remains to be one of the more active clubs in the district.
                                </p>
                                <p>
                                In 1953, RC Malolos sponsored the Rotary Club of Puerto Princesa. Through the initiative of PP Maxie Valenzuela, RC Malolos started to expand Rotary to outlying cities and Municipalities with the organization of the Rotary Club of Caloocan in 1959. Another 11 clubs were organized by RC Malolos in Bulacan, they are the following:
                                </p>
                                <p>
                                1971 - RC Baliwag			1981	- RC Plaridel<br/>
                                1974	- RC Valenzuela		1982	- RC Balagtas<br/>
                                1974	- RC Meycauayan		1984	- RC Bulakan<br/>
                                1976	- RC Sta. Maria		1994	- RC Barasoain<br/>
                                1980	- RC Hagonoy		2000	- RC Malolos Hiyas<br/>
                                1980	- RC Bocaue

                                </p>
                                <p>
                                RC Malolos produced four (4) district governors, namely:
                                <br/><br/>
                                1970-1971	PDG Sabino “Benny” Santos for D-380, he later became RI Director in 1979, served in the RI Board in 1989-1991, and also became a chair of PCRG
                                <br/>1987-1988	PDG Francisco “Jun” Aniag, Jr., the first governor of District 3770, he later served as chair of PCRG
                                <br/>1992-1993	PDG Pacifico “Boy” Aniag
                                <br/>2016-2017	PDG George V. Bunuan
                                </p>
                                <p>
                                In 1978, when RI was looking for a worldwide project through its 3-H (Health, Hunger and Humanity) Committee, PRID Benny Santos wrote RI that if Rotary could provide the vaccine, they would mobilize all the Rotarians in the entire Philippines and immunize all the children. It was approved and some 6 million children were immunized. It was a huge success hence RI decided in 1985 to implement Polio Plus globally
                                </p>
                                <p>
                                RC Malolos played a critical role in the pilot polio eradication program in the Philippines and contributed to the success of the “greatest humanitarian program” of Rotary.
                                </p>
                                <p>
                                In October 2019 at the age of 96, Past RI President Mat Caparas joined and was inducted as a member of the Rotary Club of Malolos, he was the only Filipino that became RI President. He actually grew up in Malolos so his joining of RC Malolos was actually a homecoming. He is a charter member of the Rotary Club of Caloocan that was sponsored by RC Malolos in 1959.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                      <div class="col-lg-6 col-md-6">

                                <img src="img/real/new/districtlogo.png" alt="Image" style="width:100%">
                        </div>

                        <div class="col-lg-6 col-md-6">

                                <img src="img/real/new/logo.jpg" alt="Image" style="width:100%">
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->

                      <div class="fact">
                          <div class="container-fluid">
                              <div class="row counters">
                                  <div class="col-md-6 fact-left wow slideInLeft">
                                      <div class="row">
                                          <div class="col-6">
                                              <div class="fact-icon">
                                                  <i class="flaticon-worker"></i>
                                              </div>
                                              <div class="fact-text">
                                                  <h2 data-toggle="counter-up">1</h2>
                                                  <p>INCREASE OUR IMPACT</p>
                                              </div>
                                          </div>
                                          <div class="col-6">
                                              <div class="fact-icon">
                                                  <i class="flaticon-building"></i>
                                              </div>
                                              <div class="fact-text">
                                                  <h2 data-toggle="counter-up">2</h2>
                                                  <p>EXPAND OUR REACH</p>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-md-6 fact-right wow slideInRight">
                                      <div class="row">
                                          <div class="col-6">
                                              <div class="fact-icon">
                                                  <i class="flaticon-address"></i>
                                              </div>
                                              <div class="fact-text">
                                                  <h2 data-toggle="counter-up">3</h2>
                                                  <p>ENHANCE PARTICIPANT ENGAGEMENT</p>
                                              </div>
                                          </div>
                                          <div class="col-6">
                                              <div class="fact-icon">
                                                  <i class="flaticon-crane"></i>
                                              </div>
                                              <div class="fact-text">
                                                  <h2 data-toggle="counter-up">4</h2>
                                                  <p>INCREASE OUR ABILITY TO ADOPT</p>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>



            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                     <div class="section-header text-center">
                        <p>(Lyrics Ricardo “Leony” Liongson)<br/> (Music: PP Jose “JB”Robles)</p>
                        <h2>RISE MALOLOS ROTARY</h2>
                    </div>

                    <div class="about-text text-center">
                        <p>
                            We’ve set our shoulders to the wheels<br/>
                            Rotary aim to fulfill<br/>
                            For men’s uplift strive hard we will<br/>
                            And his lot we’ll champion still<br/><br/><br/>

                            Our life’s work we’ll dignify<br/>
                            With norms that are sublime and high<br/>
                            Cave thou a name in the history<br/>
                            Rise! Malolos Rotary<br/><br/><br/>

                            Will spring off love, faith and good cheer<br/>
                            In a world be set with fear<br/>
                            Our voices ring out for all to hear<br/>
                            Thy precepts we hold so clear<br/><br/><br/>

                            Peace in our time our fondest dream<br/>
                            That joy may flow in the endless stream<br/>
                            Keep thou thy tryst with destiny <br/>
                            Rise Malolos Rotary
                        </p>
                    </div>
                </div>
            </div>

            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                        <div class="section-header text-center">
                        <p>(Lyrics: PP Ricardo “Carding” Nicolas) </p>
                        <h2>TINDIG MALOLOS ROTARY</h2>
                    </div>
                    <div class="about-text text-center">
                    <p>
                    Lahat ay naninindigan<br/>
                    Rotary’y magtagumpay<br/>
                    Kapwa tao at tulungan<br/>
                    Paglingkuran ang tanan<br/><br/><br/>

                    Mamuhay ng Marangal,<br/>
                    Gawa’y tumpak, wasto’t banal<br/>
                    Sila muna bago kami <br/>
                    Tindig! Malolos Rotary. (2x)
                    </p>
                    </div>
                </div>
            </div>


            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="img/members/alex.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="section-header text-left">
                                <p>RC MALOLOS PRESIDENT RY2020-2021</p>
                              <h2>Alex Paulino</h2>
                            </div>
                            <div class="about-text">
                                <p>
                                These are challenging times, and I am grateful to each of us for the efforts in fighting COVID-19. No challenge is too big for Rotarians. The bigger the challenge, the more passionate the Rotarian.
                                Look at what we can do when we take on different projects. Look at the millions of lives we improve by strengthening access to health, environment and education. Our basic education and literacy programs have nation-building impact.
                                The biggest gift we are giving is the power to touch a life, imagine the change we, as Rotary members, can make when there are so many more of us! More people to care for others, more people to Serve to Change Lives. Think of the impact we can have through grow more, do more. More members will enable us to embark on bigger and bolder service projects. And each of us can also continue to serve in our own personal ways, responding to needs in our communities.
                                Let us challenge ourselves to do  projects and programs that have national reach and impact. Let us Serve to Change Lives. ..
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="img/real/new/Shekhar-Mehta.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="section-header text-left">
                                <p>RI PRESIDENT RY2020-2021</p>
                              <h2>Shekhar Mehta</h2>
                            </div>
                            <div class="about-text">
                                <p>
                                 I wish each of you and your families a great Rotary New Year! Together, let us make it the best year of our lives, by making it a year to grow more and do more. Let this be a year of changemakers, and let us begin with our membership.
                                 That is precisely why the Each One, Bring One initiative is so important. During this year, I urge you to dream of new ways in which Rotary can expand its reach into your community and therefore the world. If each member introduces one person to Rotary, our membership can increase to 1.3 million by July 2022. So, let’s just do it!
                                 Imagine the change we, as Rotary members, can make when there are so many more of us! More people to care for others, more people to Serve to Change Lives. Think of the impact we can have through grow more, do more. More members will enable us to embark on bigger and bolder service projects. And each of us can also continue to serve in our own personal ways, responding to needs in our communities.
                                 The beauty of Rotary is that service means different things to different people around the world. One element, however, that we can incorporate into all of our service initiatives is empowering girls. Unfortunately, even in this day and age, girls and young women face disproportionate challenges all over the world. We have the power to lead the charge for gender equality. Empowering girls and young women to have greater access to education, better health care, more employment, and equality in all walks of life should be embedded in
                                 every Rotary project we launch. Girls are future leaders, so we must ensure that we help them shape their future.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row align-items-center wow fadeInUp">
              <div class="col-md-12">

                        <center><img src="img/4-way-test.jpg" alt="Image" style="width:50%"></center>
                </div>
            </div>


            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>
</html>
