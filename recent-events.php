<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Recent Events | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

            <!-- Single Post Start-->
            <div class="single">
                <div class="container">
                    <h2>Recent Events for 2022-2023</h2>
                    <div class="row">
                        <div class="accordion" id="accordionExample">
                              <!-- 75th Founding Anniversary and Induction Ceremonies -->
                              <div class="accordion-item">
                                <h2 class="accordion-header" id="heading20">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse20" aria-expanded="false" aria-controls="collapse20">
                                    <strong>75th Founding Anniversary and Induction Ceremonies</strong>
                                    </button>
                                </h2>
                                <div id="collapse20" class="accordion-collapse collapse"
                                aria-labelledby="heading120" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="container">
                                            <div class="row">
                                                <?php
                                                renderImage(101, '75th-founding-anniversary', 'fa');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 1st Governor's Visit -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading19">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse19" aria-expanded="false" aria-controls="collapse19">
                                    <strong>1st Governor's Visit</strong>
                                    </button>
                                </h2>
                                <div id="collapse19" class="accordion-collapse collapse"
                                aria-labelledby="heading19" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="container">
                                            <div class="row">
                                                <?php
                                                renderImage(7, '1st-governor-visit', 'gv');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- TRF Recognition Night Ry 2022-2023 at Diamond Hotel, Manila< -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading18">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse18" aria-expanded="false" aria-controls="collapse18">
                                    <strong>TRF Recognition Night Ry 2022-2023 at Diamond Hotel, Manila</strong>
                                    </button>
                                </h2>
                                <div id="collapse18" class="accordion-collapse collapse"
                                aria-labelledby="heading18" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="container">
                                            <div class="row">
                                                <?php
                                                renderImage(12, 'trf-recognition-night', 'trn');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <!-- TRF Grant Management, District Membership & Public Image Learning Seminars -->
                             <div class="accordion-item">
                                <h2 class="accordion-header" id="heading17">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                    <strong>TRF Grant Management, District Membership & Public Image Learning Seminars</strong>
                                    </button>
                                </h2>
                                <div id="collapse17" class="accordion-collapse collapse"
                                aria-labelledby="heading17" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="container">
                                            <div class="row">
                                                <?php
                                                renderImage(7, 'trf-grant-management', 'tgm');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <!-- 73rd Charter Anniversary, Turnover & Induction Ceremonies -->
                             <div class="accordion-item">
                                <h2 class="accordion-header" id="heading16">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                    
                                    <strong>73rd Charter Anniversary, Turnover & Induction Ceremonies</strong>
                                    </button>
                                </h2>
                                <div id="collapse16" class="accordion-collapse collapse"
                                aria-labelledby="heading16" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="container">
                                            <div class="row">
                                                <?php
                                                renderImage(13, '73rd-charter-anniversary', 'ca');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <!-- Pagsasalin ng tungkulin ng Gobernador at Opisyal ng Rotary International D3370 -->
                             <div class="accordion-item">
                                <h2 class="accordion-header" id="heading15">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                    
                                    <strong>Pagsasalin ng tungkulin ng Gobernador at Opisyal ng Rotary International D3370</strong>
                                    </button>
                                </h2>
                                <div id="collapse15" class="accordion-collapse collapse"
                                aria-labelledby="heading15" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="container">
                                            <div class="row">
                                                <?php
                                                renderImage(6, 'pagsasalin-tungkulin', 'pt');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <!-- DISTAS -->
                             <div class="accordion-item">
                                <h2 class="accordion-header" id="heading14">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                    <strong>DISTAS</strong>
                                    </button>
                                </h2>
                                <div id="collapse14" class="accordion-collapse collapse"
                                aria-labelledby="heading14" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="container">
                                            <div class="row">
                                                <?php
                                                renderImage(3, 'distas', 'd');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- CLUB LEADERSHIP PLAN -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                       <strong> Club Planning 2023</strong>
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse"
                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <div class="container">
                                            <div class="row">
                                                <?php
                                                renderImage(9, 'club-leadership-plan', 'clp');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Visit of Dist. Gov. Nam Jae-Ho, D3740 Rotary Club of Jincheon Bongwha, Korea -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                
                                <strong>Visit of Dist. Gov. Nam Jae-Ho, D3740 Rotary Club of Jincheon Bongwha, Korea</strong>
                                </button>
                                </h2>
                                <div id="flush-collapseTwo" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(5, 'visit-of-d3740', 'vod');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- GIFT GIVING - STATIONARY WITH D3740 GOVERNOR NAM JAE-HO AND ASSISTANT GOVERNORS AT PITPITAN ELEMENTARY SCHOOL -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="true" aria-controls="flush-collapseThree">
                                
                                <?php renderTable("Gift Giving - Stationary with D3740 Governor Nam Jae-Ho and Assistant Governors at Pitpitan Elementary School","April 3, 2023","IWC Malolos","100 Students"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseThree" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(8, 'gift-giving-pitpitan-school', 'ggd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- GIFT GIVING – GROCERIES AND RICE WITH D3740 GOVERNOR NAM JAE-HO AND ASSISTANT GOVERNORS AT NIA ROAD, PITPITAN-->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingThree3">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree3" aria-expanded="true" aria-controls="flush-collapseThree3">
                                
                                <?php renderTable("Gift Giving - Groceries with D3740 Governor Nam Jae-Ho and Assistant Governors at Nia Road, Pitpitan","April 3, 2023","IWC Malolos","103 Families"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseThree3" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingThree3" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(6, 'gift-giving-groceries', 'ggd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Animal Dispersion Program of the Provincial Gov't of Bulacan where RC Malolos announced its project of distributing disinfectants agianst African Swine Fever Virus (ASFV) for backyard pig farms in Bulacan. -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingFour">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                                <?php renderTable("(GGP) Safe and Effective Disinfection of Small Pig Farms Against African Swine Fever Virus (ASFV)","March 14, 2023","","325 Small Pig Farm"); ?>  
                            </button>
                                </h2>
                                <div id="flush-collapseFour" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(16, 'swine-fever-disinfection', 'sfd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- PAMASKONG HATID SA MGA BATANG MAY KAPANSANAN -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingFive">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                                <?php renderTable("Gift Giving Project - Akapin Foundation","December 19, 2022","IWC Malolos","70 PWD Children"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseFive" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(22, 'pamaskong-hatid-kapansanan', 'phk');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- World Polio Day Celebration particpated by the 7 Rotary Clubs in the City Malolos -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingSix">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false" aria-controls="flush-collapseSix">
                                <?php renderTable("World Polio Day - Funwalk, Unveiling of End Polio Banner and Ceremonial Patak","October 24, 2022","Rotary Club's in Malolos","3 Babies"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseSix" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingSix" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(27, 'world-polio-day', 'wpd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Global Handwashing Day - Distribution of Hygiene Kit to San Pablo Elementary, City of Malolos in partnership with Inner Wheel Club of Malolos and Inner Wheel Club of Malolos North.  -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingSeven">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSeven" aria-expanded="false" aria-controls="flush-collapseSeven">
                                <?php renderTable("Hand Washing Project","August 25, 2022","Vice Mayor's Office and IWC Malolos","San Pablo Elementary School (305 Students)"); ?>    
                            </button>
                                </h2>
                                <div id="flush-collapseSeven" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingSeven" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(12, 'global-handwashing-day', 'ghd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Livelihood Training: Buchi & Chicken Nuggets Making   -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingEight">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseEight" aria-expanded="false" aria-controls="flush-collapseEight">
                                <?php renderTable("Livelihood Seminar - Buchi and Chicken Nuggets Making","September 17, 2022","IWC Malolos and IWC Malolos North","26"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseEight" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingEight" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(5, 'livelihood-training-buchi', 'lt');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Livelihood Training: Dishwashing Liquid & Fabric Conditioner   -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingEight8">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseEight8" aria-expanded="false" aria-controls="flush-collapseEight8">
                                <?php renderTable("Livelihood Seminar - Powder Detergent Dishwashing Liquid and Fabric Conditioner","September 17, 2022","IWC Malolos and IWC Malolos North","20"); ?>

                                </button>
                                </h2>
                                <div id="flush-collapseEight8" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingEight8" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(6, 'livelihood-training-detergent', 'lt');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- School Supplies - Turn Over (ALS - Malolos Market) -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingNine">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine" aria-expanded="false" aria-controls="flush-collapseNine">
                                <?php renderTable("School Supplies - Turn Over (ALS - Malolos Market)","September 9, 2022","IWC Malolos and IWC Malolos North","25"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseNine" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingNine" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(2, 'school-supplies-turnover-als-malolos-market', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- School Supplies - Turn Over (ALS - City Jail) -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingNine9">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine9" aria-expanded="false" aria-controls="flush-collapseNine9">
                                <?php renderTable("School Supplies - Turn Over (ALS - City Jail)","September 9, 2022","IWC Malolos and IWC Malolos North","30"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseNine9" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingNine9" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(3, 'school-supplies-turnover-als-city-jail', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- School Supplies - Turn Over (ALS - Tanglaw Pag-asa) -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingNine99">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine99" aria-expanded="false" aria-controls="flush-collapseNine99">
                                <?php renderTable("School Supplies - Turn Over (ALS - Tanglaw Pag-asa)","September 9, 2022","IWC Malolos and IWC Malolos North","20"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseNine99" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingNine99" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(4, 'school-supplies-turnover-als-tanglaw-pagasa', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- School Supplies - Turn Over (ALS - Provincial Jail) -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingNine999">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine999" aria-expanded="false" aria-controls="flush-collapseNine999">
                                <?php renderTable("School Supplies - Turn Over (ALS - Provincial Jail)","September 9, 2022","IWC Malolos and IWC Malolos North","35"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseNine999" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingNine999" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(3, 'school-supplies-turnover-als-provincial-jail', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Donation of School Suppplies to Caliligiwan Elementary School -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingTen">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTen" aria-expanded="false" aria-controls="flush-collapseTen">
                                <?php renderTable("School Supplies - Turn Over (Caliligiwan Elementary School)","September 9, 2022","IWC Malolos and IWC Malolos North","48 Students"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseTen" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingTen" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(8, 'school-supplies-turnover-caliligawan', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Blood Donation Program in cooperation with the Office of the Vice Mayor, City of Malolos -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingEleven">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseEleven" aria-expanded="false" aria-controls="flush-collapseEleven">
                                
                                <?php renderTable("Blood Donation Drive","August 16, 2022","Vice Mayor's Office","Philippine Red Cross (30 Donors)"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseEleven" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingEleven" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(22, 'blood-donation', 'bd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Mangrove Planting at Bgy. Babatnin, City of Malolos in partnership with United Architects of the Philippines, Bulacan Chapter -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingTwelve">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwelve" aria-expanded="false" aria-controls="flush-collapseTwelve">
                               
                                <?php renderTable("Bakawan Planting at Brgy. Babatnin","July 30, 2022","Architech Association of the Philippines, Bulacan Chapter","Coastal Area of Malolos"); ?>    
                            </button>
                                </h2>
                                <div id="flush-collapseTwelve" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingTwelve" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(26, 'mangrove-planting', 'mp');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Anti-Rabies Vaccination  -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading13">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse13" aria-expanded="false" aria-controls="flush-collapse13">
                                <?php renderTable("Free Capon and Anti-Rabbies for Dogs and Cats","July 8, 2022","Vice Mayor's Office","70"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapse13" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading13" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(16, 'libreng-kapon', 'lk');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Needs Assessment meeting 
                            with Green Antz Builders Inc. & Bgy. Capt. Belty Bartolome of Bgy. Pamarawan regarding handling of waste plastics.  -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading14">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse14" aria-expanded="false" aria-controls="flush-collapse14">
                                Needs Assessment meeting with Green Antz Builders Inc. & Bgy. Capt. Belty Bartolome of Bgy. Pamarawan regarding handling of waste plastics.
                                </button>
                                </h2>
                                <div id="flush-collapse14" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading14" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(18, 'needs-assesment-meeting', 'nam');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Post End-->
            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Recent Events";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
