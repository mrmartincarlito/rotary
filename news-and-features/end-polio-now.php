<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>News & Features (End Polio Now) | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
        <style>
        .news-title {
            font-size: 24px;
            font-weight: bold;
            margin-bottom: 10px;
        }
        .news-date {
            font-size: 14px;
            color: #999;
            margin-bottom: 10px;
        }
        .news-content {
            font-size: 16px;
            line-height: 1.6;
        }
        .image-container {
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .news img {
            width: 65%;
        }
        .excerpt {
            color: red;
            font-size: 18px;
            font-weight: 600;
        }
        .news a {
            color: blue;
        }
        .history-polio img {
            width: 100%;
        }
    </style>
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

             <!-- Single Post Start-->
             <div style="padding-top:0px;"class="single">
                <div class="about wow fadeInUp" data-wow-delay="0.1s">
                    <div class="container">
                        <div class="row">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#one"><b>100 Years of Miracle</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#two"><b> Rotary's Involvement in Polio Eradication</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#three"><b>Rotary speaker Caparas led fight to eradicate polio</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#four"><b>A History of Polio Eradication</b></a>
                                </li>
                            </ul>
                        </div>
                        <!-- Tab Content -->
                        <div class="row">
                            <div class="tab-content">
                                <div id="one" class="tab-pane fade show active">
                                    <div class="news">
                                        <h2 class="news-title mt-5">100 YEARS OF MIRACLE</h2>
                                        <p class="news-date"><i class="far fa-calendar mr-2"></i>Oct. 24, 2009</p>
                                            <div class="container news-content">

                                            <div class="row">
                                                <?php
                                                // Path to the folder containing the images
                                                $imageFolder = '../img/news-and-features/end-polio-now/';

                                                // Get an array of image filenames from the folder
                                                $imageFiles = glob($imageFolder . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
                                                // Display the images
                                                foreach ($imageFiles as $imageFile) {
                                                ?>
                                                <div class="col-md-12">
                                                    <div class="image-container">
                                                        <img src="<?php echo $imageFile; ?>" alt="Image" class="img-fluid">
                                                    </div>
                                                </div>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="two" class="tab-pane fade">
                                    <div class="news">
                                        <h2 class="news-title mt-5">ROTARY'S INVOLVEMENT IN POLIO ERADICATION</h2>
                                        <p class="news-date"><i class="far fa-calendar mr-2"></i>2005<br>
                                        [The program to eradicate polio, called Polio Plus, has been Rotary International’s primary effort for over 20 years.  This speech was given to several Rotary clubs who asked if I would relate how Rotary International actually became involved in such a worldwide humanitarian effort.] </p>
                                        <p class="news-content">Rotary’s Polio Plus program has been described as the finest humanitarian project by a non-governmental organization the world has ever known.   Rotary has been nominated for the Nobel Peace Prize for our efforts.  It is Rotary’s project of the highest interest for over 20 years.<br><br>
                                        Many Rotarians have no idea of how Rotary ever became involved in eradicating polio in the world.  So, let me recall the story.<br><br>
                                        In the 1950s, 60s, and 70s, virtually every person knew someone in their family or circle of friends who had polio.  In the early 1950s, there were annually over 55,000 cases of polio in the United States.  Worldwide there were perhaps 500,000 cases of polio.  Of that number 50,000 children a year would die from polio and thousands more would be crippled, paralyzed or suffer lifelong disabilities. <br><br>
                                        So that was the backdrop of our story.  In 1978, Rotary had a committee, appointed by R.I. President Clem Renouf, to design a new direction for Rotary. It was called the Health, Hunger and Humanity Committee.  This was a small committee to design a program for Rotary International to undertake projects far greater than any club or district could do.  Rotary had never undertaken a corporate or worldwide project – just club programs.  I happened to be co-chairman of that 3-H Committee. We knew that if we didn’t have an immediate success, the Rotary world would probably scrap the program the next year.  So we looked for an “immediate success” project.  We had about 16 projects proposed from around the world.  One proposal was from the Philippines.  Dr. Benny Santos wrote that if Rotary could provide the vaccine, they would mobilize all the Rotarians in the entire Philippines and immunize all the children.  So, that was it.  We approved the project; and if I recall correctly, some 6 million children were immunized against polio.  It was a huge success.  Pharmaceutical companies had other types of vaccine – for measles, tetanus, chicken pox and other vaccines which were donated for Rotarians to distribute in several areas of the world.  Rotary proved that immunization was the kind of project Rotary volunteers could handle. <br><br>
                                        A couple years passed, and another Rotary committee was created in 1982 by R.I. President Stan McCaffrey called the New Horizons Committee.  This group had the job of “looking into the future of Rotary to see what tasks or new directions Rotary could take on the future.”  I happened to be chairman of this committee.  We considered hundreds of ideas – some big ones and some rather frivolous.  Finally, I suggested that we ought to be thinking 20 or 30 years into the future.  Why not do something big for Rotary’s 100th anniversary coming up in 2005?  A letter from Rotarian John Sever suggested that we might provide polio vaccine for all the children in the world.  The committee thought that was a good idea, so it was one of the 35 suggestions to the R.I. Board of Directors.  So, in l982 the Board of Rotary International approved the idea of giving polio vaccine to all the children in the world.  The project was called “Polio 2005.” <br><br>
                                        Two or three years went by, and finally in 1985, Dr. Carlos Canseco, RI Pres. from Monterrey, Mexico said that if we were going to get the task done by 2005, we should get started.  So, he called Dr. Albert Sabin to Evanston and we had a meeting of some of the world’s most distinguished medical and public health leaders.   Dr. Sabin said it would cost at least $100 million dollars and we would have to immunize 500 million children.  Wow what a job! <br><br>
                                        So, Rotary set a goal of $ 120,000 to raise the funds, and the name of the project was changed to “Polio Plus.”  It was the first major fund raising campaign by Rotarians of the world for a single project.  However, by 1987 we had surpassed the goal and actually raised $240 million.   So, Rotary leaders went to the World Health Organization and said we want to eradicate polio.  It was not well accepted by all the WHO leaders who represented some of the most knowledgeable health authorities in the world. Rotary was “just a service club.”  Finally, when Rotary told them that we had over a million volunteers and $247 million in our pocket, they said, “Come on in.”  So we became full partners of the World Health Organization, UNICEF, and the US Centers for Disease Control. <br><br>
                                        At that time, in 1988, you could find polio in 125 nations of the world and it was estimated that there were 350,000 cases of polio in the world every year.  But we took on the project – one country at a time.  Our first big immunization day was in Mexico, where we immunized 13 million children.  Then we went to Central America and South America.  One nation after another became “polio free.” <br><br>
                                        Rotary Clubs became “Polio Plus Partners” to raise funds for National Immunization Days.  The Partners purchased ice boxes, colorful vests, caps, leaflets, street banners and many other items needed to mobilize whole nations to immunize their children.   <br><br>
                                        Mary Elena and I were in India to participate in the national day of immunization.  There were banners on the streets, parades, notices, distribution of thousands of radio and television announcements, plus hand bills and leaflets.  In that one day over 125 million children received the two drops of polio vaccine.  We have gone to some of the most poverty stricken areas of the Philippines, Ethiopia, Turkey and other nations to assist in National Immunization Days.  <br><br>  
                                        The project is an amazing and complicated one. Rotarians and health workers have to go to the most remote areas of the world by canoe, camels, elephants, horseback, motorbikes, and every other conceivable vehicle to reach all the world’s children.   <br><br>
                                        I remember sitting on a rock on the side of a dusty road in Argentina giving vaccine to children in a remote area.  Dozens of volunteer Rotarians, youth exchange students, health workers, youth organizations and other would go door to door urging parents to bring their children to get the vaccine.<br><br> 
                                        Even in China, Laos, Vietnam, Cuba, Myanmar and other areas where there is no Rotary, we worked freely to distribute the vaccine.  An interesting experience occurred in China.  There was reluctance by China officials to accept vaccine from the Western Nations.  We said, use Chinese made vaccine. But there was no polio vaccine made in China.  So, Rotary said, “We will build a pharmaceutical plant in China, and made a grant of $18 million dollars to construct a factory in China.   As soon as the project was under way, China said now we will take the vaccine from the West.  Immediately, China said all the children of China will be immunized, and the first two days, 100 million Chinese youngsters received the polio vaccine. <br><br>
                                        The amazing thing is that the pharmaceutical plant has never produced one dose of polio vaccine, and the Chinese officials went ahead and immunized their children when they saw that Rotary International was serious about this mission. <br><br>
                                        We were not able to meet the target of a polio-free world by 2005, but we are close.  Over 99% of the children of the world have received polio vaccine. You can find polio only in 4 nations, not the 125 countries when we started.  We hope that we can soon stop the incidents in Nigeria, India, Pakistan andAfghanistan.  Last year there were only about a thousand cases in the world – compared to the 350,000 cases a year when Rotary undertook this humanitarian mission. <br><br>
                                        There are so many aspects to the polio story.  I recall one day, when I was Chairman of The Rotary Foundation.  We had a call from the World Health Organization, saying that they had arranged for a four day cease fire in the civil war in the Sudan so we could go in and immunize their children.  But the WHO did not have any money to purchase the vaccine.  I said, “How much does it take?”  It was going to cost $400,000.  I said, “You can have it immediately from The Rotary Foundation.”  The Chairman can make a grant up to $500,000 for an emergency humanitarian effort. The war stopped, health workers went in and immunized 3 or 4 million children – then the war started again.  The World Health Organization said, “Of all the places in the world, there was no place we could turn – except Rotary, to make that project happen.”  Clearly, without the efforts of Rotary the achievements to eradicate polio would never have happened. <br><br>
                                        Perhaps some of you have participated in a National Immunization Day. It is an amazing experience.  I remember when Mary Elena and I sloshed through the mud streets of a poverty stricken village in Ethiopia – one of the poorest nations of the world.  The homes were nothing but shacks, dirt floors, which turned into mud with the occasional showers.  Families cooked on wood fires outside their huts.  This was the first polio immunization in this poor nation. In those few days, nearly 10 million children were given the drops of polio vaccine.  <br><br> 
                                        But of all the experiences of that day, I recall the ceremony to start the immunization.  The President of Ethiopia was on hand.  There were about a hundred little children lined up to receive the vaccine from their national president.   And just at that time, on the other side of the room were about 35 small children, perhaps 5 to 10 years of age, all in wheelchairs, or leg braces, or crutches -- they were all polio victims with bent backs and withered arms and twisted legs.  They stood up the best they could and sang a song to the President.  The song said: “It’s too late for us – but don’t let other children get polio.  Do what you can to Kick Polio Out of Africa.”   <br><br>
                                        If you had been there to hear that song, “It’s too late for us, but don’t let other children get polio,” you would know why Rotary has taken on this monumental task.  <br><br>  
                                        If we had only had the vaccine 2, 5, 10 years before in Ethiopia these children would be walking, running and playing as children want to do.   There are over 2 billion children who have received Rotary’s polio vaccine – and they are now living a life without the fear of paralysis and death from polio. <br><br>
                                        We are on the verge of eradicating this dreaded disease.  Perhaps next year, or the year after.   And the amazing thing is it has been made possible because Rotary Clubs, like yours, took a huge step some 20 years ago.   Even today, funding is necessary.  You may have heard that the Bill and Melinda Gates Foundation has contributed $350 million dollars to our efforts in recent months because they believe that Rotary will achieve this dream. <br><br>
                                        That’s the story of Rotary’s involvement in our greatest humanitarian program – Polio Plus.  And I thank every one of you who have been a part of this program for so many years.<br><br>
                                        The leaders of the world have clearly expressed that without Rotary International, this monumental achievement would never be accomplished.  We will eradicate polio in the world – and it will happen only because Rotary made a commitment some 25 years ago.  And the world has learned that Rotarians keep their promises. 
                                        </p>
                                        <p class="news-content excerpt">(Excerpt from As I Was Saying by RGHF member Cliff Dochterman, President, Rotary International, 1992-93) </p>
                                        <a target="_blank" href="http://www.rotaryfirst100.org/presidents/1992dochterman/images/PolioPlus%20Handbook1987RCMabalacat.pdf"><u><b>PolioPlus in the Philippines - 1987 PDF</b></u></a>
                                    </div>
                                </div>
                                <div id="three" class="tab-pane fade">
                                    <div class="news">
                                    <h2 class="news-title mt-5">ROTARY SPEAKER CAPARAS LED FIGHT TO ERADICATE POLIO</h2>
                                    <p class="news-date"><i class="far fa-calendar mr-2"></i>April 15, 2005<br>
                                    By Bayne Hughes
                                    DAILY Staff Writer
                                    hughes@decaturdaily.com · 340-2432</p>
                                    <p class="news-content">M.A.T. Caparas, in Decatur for a Rotary International conference, has been to more than 30 countries. He has met with religious and political leaders from around the world, including the late Pope John Paul II.<br><br>
                                    The 81-year-old Philippines native, president of Rotary International in 1985 and 1986, said the source of his greatest pride is Rotary's fight against polio.<br><br>
                                    During his tenure as president, Caparas led the organization to locate its headquarters in Evanston, Ill. He also started a two-year effort to raise $130 million. The organization surpassed the goal by $70 million.<br><br>
                                    Caparas flew to Decatur on Thursday for the Rotary Club District 6860 Conference to speak on behalf of International President Glenn Estes of Birmingham.<br><br>
                                    Decatur's two Rotary Clubs and Rotary Clubs in Athens, Cullman, Hartselle and Lawrence County are the hosts of the conference, which began Thursday.<br><br>
                                    Fifty years ago this week, Dr. Jonas Salk's polio vaccine was declared safe and effective.<br><br>
                                    "Our greatest achievement is in polio," said Caparas, who will speak at today's noon luncheon at Holiday Inn.<br><br>
                                    "If we can eradicate polio," Caparas said, "all countries will benefit. Countries that spend money (on polio) will save money. We will save hundreds of thousands of people the misery from polio."<br><br>
                                    Caparas called polio a "very personal issue" as he saw friends and their children afflicted with the disease. The virus that causes polio enters the body through the mouth. It can cause paralysis of the arms or legs, and it can be deadly if it paralyzes the muscles that help a person breathe.<br><br>
                                    Now a retired attorney, Caparas said a law partner got polio and walked with a limp. Another partner's two children got the disease and almost died.<br><br>
                                    "We think they got the virus in a swimming hole, where my children also swam," Caparas said. "So my children very well could have gotten the virus, too."<br><br>
                                    Rotary International, in cooperation with the World Health Organization, the U.S. Centers for Disease Control and Prevention and UNICEF, began Polio Plus, a coalition with the goal of eradicating polio by 2005.<br><br>
                                    According to Caparas, immunization is a step toward elimination because it keeps carriers from spreading polio. If 75 percent to 80 percent of the world's population receives the immunization, he believes that would kill the disease. So far, children in 128 countries have received the immunization. Rotary donated more than $600,000 to the cause.<br><br>
                                    Rotary and Polio Plus did not, however, reach their 2005 goal. Caparas said the Middle Eastern areas around India and Pakistan and the central African areas around Nigeria and the Congo are still hot spots.<br><br>
                                    Two years ago, rumors in Muslim Nigeria that the vaccine would sterilize children halted immunization efforts.<br><br>
                                    "Only after we had the vaccine tested by an independent lab would they allow us to start immunizations again," Caparas said. "In India, they're vaccinating people by the millions."<br><br>
                                    Caparas estimated there were more than 400,000 cases of polio a year before Polio Plus began. Rotary estimated that number dropped to 700 during the past year.<br><br>
                                    "The problem is there's always a danger as long as there's just one because that one person might migrate to another country," Caparas said. "A few years ago, they had an outbreak in Canada that they traced back to somebody from Holland."<br><br>
                                    "As long as one virus is alive, you and I are still in danger," he continued. "We've got to eliminate the virus."</p>
                                </div>
                                </div>
                                <div id="four" class="tab-pane fade">
                                    <div class="news">
                                    <h2 class="news-title mt-5">A HISTORY OF POLIO ERADICATION</h2>
                                    <p class="news-date"><i class="far fa-calendar mr-2"></i>2004</p>
                                    <div class="container">
                                        <div class="row history-polio">
                                            <div class="col-md-6">
                                                <img src="img/news-and-features/history-of-polio-1.jpg" alt="Image 1" >
                                            </div>
                                            <div class="col-md-6">
                                                <img src="img/news-and-features/history-of-polio-2.jpg" alt="Image 2" >
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
              
            </div>
            <!-- Single Post End-->

            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "End Polio Now";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
