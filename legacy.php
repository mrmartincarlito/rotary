<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Our Legacy | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">

        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>

  <body>
    <div class="wrapper">

      <!-- Nav Bar Start -->
      <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
      <!-- Nav Bar End -->
            
            
            <!-- Page Header Start -->
            <div class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                          <h2>Our Legacy</h2>
                        </div>
                        <div class="col-12">
                            <a href="">Home</a>
                            <a href="">Our Legacy</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page Header End -->



            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-md-12">
                            <div class="section-header text-center">
                                <p>Rotary Club Malolos</p>
                              <h2>OUR LEGACY</h2>

                            </div>
                            <div class="about-text text-center">
                                <h4>CLUB PROFILE</h4>

                                  <p>Club No. 16895</p>
                                  <p>Chartered July 25, 1949</p>
                                  <p>1st Club in Bulacan Province</p>
                                  <p>2nd Oldest Club in the District</p>
                                  <p>Mother Club – Rotary Club of Manila

                                     (1st Rotary Club in Asia,

                                     Chartered June 1, 1919)</p>
                            </div>
                            <div class="about-text text-center">
                            <br/><br/>
                                <h4>MEMBERSHIP PROFILE (As of March, 2021)</h4>

                                  <center>
                                    <table border=1 cellpadding="5px">
                                      <tr>
                                        <td>Regular Members</td>
                                        <td>44</td>
                                      </tr>
                                      <tr>
                                        <td>Male</td>
                                        <td>34 (77%)</td>
                                      </tr>
                                       <tr>
                                          <td>Female</td>
                                          <td>10 (23%)</td>
                                        </tr>

                                         <tr>
                                            <td>Average Age</td>
                                            <td>54 years</td>
                                          </tr>
                                           <tr>
                                            <td>Honorary Members</td>
                                            <td>3</td>
                                          </tr>
                                    </table>
                                  </center>
                                  <br/><br/>
                                  <p><i>RC Malolos started accepting women in 2018</i></p>
                            </div>



                            <div class="about-text text-center">
                            <br/><br/>
                                <h4>MEMBERSHIP Classifications</h4>

                                  <center>
                                    <table border=1 cellpadding="20px">
                                      <tr>
                                        <td>Local Gov’t Service</td>
                                        <td>7</td>
                                      </tr>
                                      <tr>
                                        <td>Trading Services</td>
                                        <td>7</td>
                                      </tr>
                                       <tr>
                                          <td>Food Services</td>
                                          <td>3</td>
                                        </tr>

                                         <tr>
                                            <td>Medical Practice</td>
                                            <td>3</td>
                                          </tr>
                                           <tr>
                                            <td>Human Resources</td>
                                            <td>3</td>
                                          </tr>
                                           <tr>
                                            <td>Construction</td>
                                            <td>2</td>
                                          </tr>
                                          <tr>
                                            <td>Education</td>
                                            <td>2</td>
                                          </tr>

                                      <tr>
                                         <td>Power and Energy</td>
                                         <td>2</td>
                                       </tr>

                                       <tr>
                                         <td>Engineering Services</td>
                                         <td>2</td>
                                       </tr>
                                      <tr>
                                         <td>Comm’l Printing</td>
                                         <td>2</td>
                                       </tr>

                                  <tr>
                                       <td>Customs Brokerage</td>
                                       <td>2</td>
                                     </tr>
                                  <tr>
                                     <td>Computer Services</td>
                                     <td>2</td>
                                   </tr>

                                <tr>
                                   <td>Others</td>
                                   <td>7</td>
                                 </tr>

                                    </table>
                                  </center>

                            </div>





                        </div>
                    </div>

                </div>
            </div>



            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                              <img src="img/members/prominent1.png" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="section-header text-left">
                              <p>PROMINENT MEMBER</p>
                              <h2>PRIP Mat Caparas</h2>
                            </div>
                            <div class="about-text">
                                <p>
                                  The 1st and only Filipino to become

                                  RI President
                                </p>

                                <p>
                                  RI President – 1986 to 1987

                                  “Rotary Brings Hope”
                                </p>

                                <p>
                                During his term, Rotary started

                                accepting female members
                                </p>

                                <p>
                                Introduced Rotary Community

                                Corps Program
                                </p>
                                <p>
                                Launched RI’s worldwide anti-POLIO

                                drive in 1979 in the Philippines
                                </p>
                                <p>
                                Charter member of RC Caloocan in

                                1959, said club was sponsored by

                                RC Malolos
                                </p>
                                <p>
                                Joined RC Malolos in Oct. 2019
                                </p>
                            </div>
                        </div>
                    </div>
                </div>



            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                              <img src="img/members/prominent2.png" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="section-header text-left">
                              <p>PROMINENT MEMBER</p>
                              <h2>PRID Sabino “Benny” Santos</h2>
                            </div>
                            <div class="about-text">
                                <p>
                                  RI Director – 1979 to 1981
                                </p>

                                <p>
                                  RI Board Member – 1989 to 1991
                                </p>

                                <p>
                                Dist. Governor – 1970 to 1971
                                </p>

                                <p>
                                Suggested to RI in 1978 for its 3-H
                                (Health, Hunger and Humanity)
                                worldwide project the POLIO
                                eradication program that was later
                                launched in the Philippines in 1979
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
                <br/><br/>
                  <div class="row align-items-center">
                      <div class="col-lg-12 col-md-12">

                                <center><img src="img/members/prominent3.png" alt="Image" style="width:80%"></center>
                        </div>

                    </div>


            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>
</html>
