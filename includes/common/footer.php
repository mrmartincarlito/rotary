<div class="footer wow fadeIn" data-wow-delay="0.3s">
    <div class="container footer-menu">
        <div class="f-menu">
            <a href="">"Service Above Self" </a>
            <a href="">"One Profits Most Who Serves Best"</a>
        </div>
    </div>
    <div class="container copyright">
        <div class="row">
            <div class="col-md-6">
                <p>&copy; <a href="http://rcmalolos.org/">rcmalolos.org</a> All Right Reserved.</p>
            </div>
            <div class="col-md-6">
                <p>Rotary Club of Malolos</p>
            </div>
        </div>
    </div>
</div>