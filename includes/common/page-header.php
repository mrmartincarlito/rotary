 <!-- Page Header Start -->
 <div class="page-header pt-4 pb-2">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <img class="mini-logo" src="../img/rcmlogo-small.png"/>
                        <h2 id="dynamic-heading">Title</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->