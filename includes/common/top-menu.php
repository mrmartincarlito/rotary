<div class="top-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
            <div class="logo d-flex align-items-center">
                <img src="../../img/rcmlogo.png" alt="logo-rotary" class="mr-2">
                <h1>Rotary Club of Malolos</h1>
            </div>
            </div>
            <div class="col-lg-6 col-md-7 d-none d-lg-block">
                <div class="row">
                    <div class="col-6">
                        <div class="top-bar-item">
                            <div class="top-bar-icon">
                                <i class="flaticon-call"></i>
                            </div>
                            <div class="top-bar-text">
                                <h3>Contact Us</h3>
                                <p>0917-123-4567</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="top-bar-item">
                            <div class="top-bar-icon">
                                <i class="flaticon-send-mail"></i>
                            </div>
                            <div class="top-bar-text">
                                <h3>Email Us</h3>
                                <p>email@rcmalolos.org</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="nav-bar">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg bg-dark navbar-dark">
            <a href="#" class="navbar-brand">MENU</a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                <div class="navbar-nav mr-auto">
                    <a href="/home" class="nav-item nav-link">Home</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">About Us</a>
                        <div class="dropdown-menu">
                            <a href="/history" class="dropdown-item">
                                History</a>
                            <a href="/club-hymn" class="dropdown-item">
                                Club Hymn</a>
                            <a href="/officers-for-ry" class="dropdown-item">
                                Officers</a>
                            <a href="/members" class="dropdown-item">
                                Members</a>
                                <!-- <a href="/roster-members" class="dropdown-item">
                                Roster of Members</a> -->
                            <a href="/prominent-members" class="dropdown-item">
                                Prominent Members</a>
                            <a href="/past-presidents" class="dropdown-item">
                                Past Presidents</a>
                            <a href="/partners" class="dropdown-item">
                                Partners</a>
                        </div>
                    </div>
                    <a href="https://www.rotary.org" target="_blank" class="nav-item nav-link">About Rotary</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Our Projects</a>
                        <div class="dropdown-menu">
                            <a href="/grants" class="dropdown-item">
                                Grants</a>
                            <a href="/water-sanitation-and-hygiene" class="dropdown-item">
                                Water, Sanitation & Hygiene</a>
                            <a href="/livelihood-and-community-development" class="dropdown-item">
                            Livelihood & Community Dev't.</a>
                            <a href="/disease-control-and-prevention" class="dropdown-item">
                                Disease Control and Prevention</a>
                            <a href="/basic-education-and-literacy" class="dropdown-item">
                                Basic Education & Literacy</a>  
                            <a href="/environment-protection" class="dropdown-item">
                                Environment Protection</a>
                            <a href="/other-community-service" class="dropdown-item">
                                Other Community Service</a>
                            <a href="/youth-service" class="dropdown-item">
                                Youth Service</a>
                            <a href="/international-service" class="dropdown-item">
                                International Service</a>
                        </div>
                    </div>
                    <a href="/recent-events" class="nav-item nav-link">Recent Events</a>
                    <a href="/rcm-by-year" class="nav-item nav-link">RCM By Year</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">News & Features</a>
                        <div class="dropdown-menu">
                            <a href="/end-polio-now" class="dropdown-item">
                               End Polio Now</a>
                        </div>
                    </div>
                    <a href="/contact-us" class="nav-item nav-link">Contact Us</a>
                </div>
                <div class="ml-auto">
                    <button type="button" class="btn" data-toggle="modal" data-target="#donateModal">
                            DONATE
                    </button>
                </div>
            </div>
        </nav>
    </div>
</div>

 <div class="modal fade" id="donateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                INsert Donation Description here <br/>
                <br/>

                BANK  : <b>NAME</b> <br/>
                ACCOUNT NAME : <b>NAME</b> <br/>
                ACCOUNT NUMBER : <b>NAME</b> <br/>

                <br/><br/>
                Thank you
            </div>
        </div>
    </div>
</div>