<?php

function renderImage($image_count, $folder_name, $image_name) {
    $imagesPerRow = 3;
    $imagesPerColumn = ceil($image_count / $imagesPerRow);

    for ($i = 1; $i <= $image_count; $i++) {
        // Open a new row if it's the start of a new row
        if (($i - 1) % $imagesPerRow === 0) {
            echo '<div class="row mt-3">';
        }

        echo '<div class="col-md-4 col-sm-6 wow fadeInUp">';
        // Output the card element with the centered image and border
        echo '<div class="card h-100 border-secondary">';
        echo '<div class="card-body d-flex align-items-center justify-content-center">';
        echo '<img class="img-fluid" src="img/rotary-images/'.$folder_name.'/' .$image_name.'-'.$i . '.jpg" alt="Image">';
        echo '</div>';
        echo '</div>';
        echo '</div>';

        // Close the row if it's the end of a row or the last image
        if ($i % $imagesPerRow === 0 || $i === $image_count) {
            echo '</div>';
        }
    }
}

function renderTable($title, $date, $partnership,  $beneficiaries) {

    echo '<div class="container">';
        echo '<div class="row">';
            echo '<div class="col-6"><b>'.$title.'</b></div>';
                echo '<div class="col align-self-end">';
                    echo '<div  class="row">';
                        echo '<div class="col"> <strong>Date<br></strong>'. ($date !== '' ? $date : '-') .'</div>';
                        echo '<div class="col"> <strong>Partners<br></strong>'. ($partnership !== '' ? $partnership : '-') .'</div>';
                        //echo '<div class="col"> <strong>Beneficiaries<br></strong>'. ($beneficiaries !== '' ? $beneficiaries : '-') .'</div>';
                    echo '</div>';
                echo '</div>';
        echo '</div>';
    echo '</div>';
}




