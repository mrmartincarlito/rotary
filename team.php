<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Our Members | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
        <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:
        wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
    <div class="wrapper">
      <!-- Nav Bar Start -->
      <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
      <!-- Nav Bar End -->
        <!-- Page Header Start -->
        <div class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2>Our Members</h2>
                    </div>
                    <div class="col-12">
                        <a href="">Home</a>
                        <a href="">Our Members</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->
            <!-- Team Start -->
            <div class="team">
                <div class="container">
                    <div class="section-header text-center">
                        <p>Our Team</p>
                        <h2>Meet Our Club Officer RY 2021-2022</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/alex.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>Pres. Alex Paulino</h2>
                                    <p>President</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/migs.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>PP. Migs Bautista</h2>
                                    <p>Vice President</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/leonor.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>PE. Leonor Villarmino</h2>
                                    <p>Secretary</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/coco.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>Treas. Coco Bunuan</h2>
                                    <p>Treasurer</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/andy.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>PP. Andy Del Rosario</h2>
                                    <p>Auditor</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/ronald.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>Rtn. Ronald de Leon</h2>
                                    <p>Sgt. at Arms</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/romy.jpg" alt="Team Image" style="height: 100%">
                                </div>
                                <div class="team-text">
                                    <h2>IPP. Romy Lazaro</h2>
                                    <p>Director</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/danny.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>Dir. Danny Agustin</h2>
                                    <p>Director</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/tess.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Dir. Tess Gan</h2>
                                      <p>Director</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>


                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/sahlee.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Dir. Sahiee Graida </h2>
                                      <p>Director</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/gael.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Dir. Gael Lavina</h2>
                                      <p>Director</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>


 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/leonor.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PE. Leonor Villarmino</h2>
                                      <p>President - Elect RY 2022-2023</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/setti.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PN. Setti Tanwangco</h2>
                                      <p>President Nominee RY 2023-2024</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/danny.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Dir. Danny Agustin</h2>
                                      <p>Committee chairmen - Adminstration</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/sahlee.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Dir. Sahlee Graida</h2>
                                      <p>Committee chairmen - Membership</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/tess.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Dir. Tess Gan</h2>
                                      <p>Committee chairmen - Service Projects</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/ken.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Rtn. Ken Laquindanum</h2>
                                      <p>Committee chairmen - Service Projects</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/ed.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PP. Ed Bernal</h2>
                                      <p>Committee chairmen - TRF</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/francis.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Rtn. Francis de Guzman</h2>
                                      <p>Committee chairmen - Public Image</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

 <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/dhel.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>Rtn. Dhel Liwanag</h2>
                                      <p>Committee chairmen - Youth Service</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/boy.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PDG. Boy Aniag</h2>
                                      <p>Adviser</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/members/george.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>PDG. George Bunuan</h2>
                                    <p>Adviser</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>


<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/rod.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PP. Rod Sumulong</h2>
                                      <p>Adviser</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>



<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/benjie.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PP. Benjie de Mesa</h2>
                                      <p>Adviser</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/benjie.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PP Benjie de Mesa</h2>
                                      <p>Club Trainor</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>


<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/bhey.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PP. Bhey Cunanan</h2>
                                      <p>Club Trainor</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>

<div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="team-item">
                                  <div class="team-img">
                                      <img src="img/members/setti.jpg" alt="Team Image">
                                  </div>
                                  <div class="team-text">
                                      <h2>PN. Setti Tanwangco</h2>
                                      <p>Club Trainor</p>
                                  </div>
                                  <div class="team-social">
                                      <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                      <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                      <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                      <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                  </div>
                              </div>
                          </div>


                    </div>
                </div>
            </div>
            <!-- Team End -->

             <div class="row">
               <div class="col-md-12 table-responsive">
                <div class="section-header text-center">

                    <h2>ROSTER OF MEMBERS</h2>
                  </div>
                  <center>
                  <table class="table">

                    <thead >
                        <tr>
                          <th>No.</th>
                          <th>IMAGE</th>
                          <th>NAMES</th>
                          <th>CLASSIFICATION</th>
                          <th>SPOUSE</th>
                          <th>CONTACT NO</th>
                          <th>MEMBER ID</th>
                        </tr>
                    </thead>

                    <tbody >
                              <tr class="row0">
                                <td class="column0 style0 n">1</td>
                                <td class="column1"><img src="img/members/danny.jpg" width="100px"></td>
                                <td class="column2 style0 s">AGUSTIN, DANNY</td>
                                <td class="column3 style0 s">Catering Services</td>
                                <td class="column4 style0 s">Malu</td>
                                <td class="column5 style0 n" style="font-size:0">9175935803</td>
                                <td class="column6 style0 n">10775719</td>
                              </tr>
                              <tr class="row1">
                                <td class="column0 style0 n">2</td>
                                <td class="column1"><img src="img/members/boy.jpg" width="100px"></td>
                                <td class="column2 style0 s">ANIAG, BOY</td>
                                <td class="column3 style0 s">Commercial Printing</td>
                                <td class="column4 style0 s">Alice</td>
                                <td class="column5 style0 n" style="font-size:0">9175161588</td>
                                <td class="column6 style0 n">262304</td>
                              </tr>
                              <tr class="row2">
                                <td class="column0 style0 n">3</td>
                                <td class="column1"><img src="img/members/michael.jpg" width="100px"></td>
                                <td class="column2 style0 s">AQUINO, MICHAEL</td>
                                <td class="column3 style0 s">Local Government Service</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9272028290</td>
                                <td class="column6 style0 n">10299397</td>
                              </tr>
                              <tr class="row3">
                                <td class="column0 style0 n">4</td>
                                <td class="column1"><img src="img/members/migs.jpg" width="100px"></td>
                                <td class="column2 style0 s">BAUTISTA, MIGUEL</td>
                                <td class="column3 style0 s">Entrepreneur</td>
                                <td class="column4 style0 s">EJ</td>
                                <td class="column5 style0 n" style="font-size:0">9175810555</td>
                                <td class="column6 style0 n">10299409</td>
                              </tr>
                              <tr class="row4">
                                <td class="column0 style0 n">5</td>
                                <td class="column1"><img src="img/members/ed.jpg" width="100px"></td>
                                <td class="column2 style0 s">BERNAL, ED</td>
                                <td class="column3 style0 s">Renewable Energy Mgt.</td>
                                <td class="column4 style0 s">Jean</td>
                                <td class="column5 style0 n" style="font-size:0">9178057368</td>
                                <td class="column6 style0 n">7044764</td>
                              </tr>
                              <tr class="row5">
                                <td class="column0 style0 n">6</td>
                                <td class="column1"><img src="img/members/dannyborlongan.jpg" width="100px"></td>
                                <td class="column2 style0 s">BORLONGAN, DANNY</td>
                                <td class="column3 style0 s">Fish Brokerage</td>
                                <td class="column4 style0 s">Josie</td>
                                <td class="column5 style0 n" style="font-size:0">9178009765</td>
                                <td class="column6 style0 n">2272329</td>
                              </tr>
                              <tr class="row6">
                                <td class="column0 style0 n">7</td>
                                <td class="column1"><img src="img/members/coco.jpg" width="100px"></td>
                                <td class="column2 style0 s">BUNUAN, COCO</td>
                                <td class="column3 style0 s">Computer Programming</td>
                                <td class="column4 style0 s">George</td>
                                <td class="column5 style0 n" style="font-size:0">9189082216</td>
                                <td class="column6 style0 n">6222564</td>
                              </tr>
                              <tr class="row7">
                                <td class="column0 style0 n">8</td>
                                <td class="column1"><img src="img/members/george.jpg" width="100px"></td>
                                <td class="column2 style0 s">BUNUAN, GEORGE</td>
                                <td class="column3 style0 s">Human Resource Mgt.</td>
                                <td class="column4 style0 s">Coco</td>
                                <td class="column5 style0 n" style="font-size:0">9189082215</td>
                                <td class="column6 style0 n">10299431</td>
                              </tr>
                              <tr class="row8">
                                <td class="column0 style0 n">9</td>
                                <td class="column1"><img src="img/members/juanita.jpg" width="100px"></td>
                                <td class="column2 style0 s">CABRERA, JUANITA</td>
                                <td class="column3 style0 s">Heavy Equipment Rental</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9437026426</td>
                                <td class="column6 style0 n">10775720</td>
                              </tr>
                              <tr class="row9">
                                <td class="column0 style0 n">10</td>
                                <td class="column1"><img src="img/members/lanie.jpg" width="100px"></td>
                                <td class="column2 style0 s">CAPULE, LANIE</td>
                                <td class="column3 style0 s">Travel Agency</td>
                                <td class="column4 style0 s">Ernie</td>
                                <td class="column5 style0 n" style="font-size:0">9778040148</td>
                                <td class="column6 style0 n">10440936</td>
                              </tr>
                              <tr class="row10">
                                <td class="column0 style0 n">11</td>
                                <td class="column1"><img src="img/members/rico.jpg" width="100px"></td>
                                <td class="column2 style0 s">CAPULE, RICO</td>
                                <td class="column3 style0 s">Local Government Services</td>
                                <td class="column4 style0 s">Neysa</td>
                                <td class="column5 style0 n" style="font-size:0">9173679166</td>
                                <td class="column6 style0 n">9800494</td>
                              </tr>
                              <tr class="row11">
                                <td class="column0 style0 n">12</td>
                                <td class="column1"><img src="img/members/edwin.jpg" width="100px"></td>
                                <td class="column2 style0 s">CARIASO, EDWIN</td>
                                <td class="column3 style0 s">Energy Management</td>
                                <td class="column4 style0 s">Irma</td>
                                <td class="column5 style0 n" style="font-size:0">9175330659</td>
                                <td class="column6 style0 n">6222565</td>
                              </tr>
                              <tr class="row12">
                                <td class="column0 style0 n">13</td>
                                <td class="column1"><img src="img/members/joseph.jpg" width="100px"></td>
                                <td class="column2 style0 s">CRUZ, JOSEPH</td>
                                <td class="column3 style0 s">Garments &amp; Textile Printing</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9175765261</td>
                                <td class="column6 style0 n">10440927</td>
                              </tr>
                              <tr class="row13">
                                <td class="column0 style0 n">14</td>
                                <td class="column1"><img src="img/members/felipe.jpg" width="100px"></td>
                                <td class="column2 style0 s">CRUZ, FELIPE</td>
                                <td class="column3 style0 s">Avionics Instrumentation</td>
                                <td class="column4 style0 s">Malou</td>
                                <td class="column5 style0 n" style="font-size:0">9229241065</td>
                                <td class="column6 style0 n">7044772</td>
                              </tr>
                              <tr class="row14">
                                <td class="column0 style0 n">15</td>
                                <td class="column1"><img src="img/members/tirso.jpg" width="100px"></td>
                                <td class="column2 style0 s">CRUZ, TIRSO</td>
                                <td class="column3 style0 s">IT Software Development</td>
                                <td class="column4 style0 s">Vicky</td>
                                <td class="column5 style0 n" style="font-size:0">9228635969</td>
                                <td class="column6 style0 n">8656817</td>
                              </tr>
                              <tr class="row15">
                                <td class="column0 style0 n">16</td>
                                <td class="column1"><img src="img/members/bhey.jpg" width="100px"></td>
                                <td class="column2 style0 s">CUNANAN, BHEY</td>
                                <td class="column3 style0 s">Rice Trading</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9338172180</td>
                                <td class="column6 style0 n">7044779</td>
                              </tr>
                              <tr class="row16">
                                <td class="column0 style0 n">17</td>
                                <td class="column1"><img src="img/members/francis.jpg" width="100px"></td>
                                <td class="column2 style0 s">DE GUZMAN, FRANCIS</td>
                                <td class="column3 style0 s">Commercial Printing</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9178517849</td>
                                <td class="column6 style0 n">10299445</td>
                              </tr>
                              <tr class="row17">
                                <td class="column0 style0 n">18</td>
                                <td class="column1"><img src="img/members/armand.jpg" width="100px"></td>
                                <td class="column2 style0 s">DE JESUS, ARMAND</td>
                                <td class="column3 style0 s">Sales Manager - Consultant</td>
                                <td class="column4 style0 s">Minerva</td>
                                <td class="column5 style0 n" style="font-size:0">9230898110</td>
                                <td class="column6">&nbsp;</td>
                              </tr>
                              <tr class="row18">
                                <td class="column0 style0 n">19</td>
                                <td class="column1"><img src="img/members/ronald.jpg" width="100px"></td>
                                <td class="column2 style0 s">DE LEON, RONALD</td>
                                <td class="column3 style0 s">Police-Peace &amp; Order</td>
                                <td class="column4 style0 s">Cris</td>
                                <td class="column5 style0 n" style="font-size:0">9052472858</td>
                                <td class="column6 style0 n">10299322</td>
                              </tr>
                              <tr class="row19">
                                <td class="column0 style0 n">20</td>
                                <td class="column1"><img src="img/members/benjie.jpg" width="100px"></td>
                                <td class="column2 style0 s">DE MESA, BENJIE</td>
                                <td class="column3 style0 s">Medicine Pediatrics</td>
                                <td class="column4 style0 s">Jenny</td>
                                <td class="column5 style0 n" style="font-size:0">9285077262</td>
                                <td class="column6 style0 n">262316</td>
                              </tr>
                              <tr class="row20">
                                <td class="column0 style0 n">21</td>
                                <td class="column1"><img src="img/members/andy.jpg" width="100px"></td>
                                <td class="column2 style0 s">DEL ROSARIO, ANDY</td>
                                <td class="column3 style0 s">Bakery Business</td>
                                <td class="column4 style0 s">Azon</td>
                                <td class="column5 style0 n" style="font-size:0">9085646927</td>
                                <td class="column6 style0 n">1815472</td>
                              </tr>
                              <tr class="row21">
                                <td class="column0 style0 n">22</td>
                                <td class="column1"><img src="img/members/john.jpg" width="100px"></td>
                                <td class="column2 style0 s">FONTILLAS, JOHN</td>
                                <td class="column3 style0 s">Tire Services</td>
                                <td class="column4 style0 s">TinTin</td>
                                <td class="column5 style0 n" style="font-size:0">9228709633</td>
                                <td class="column6 style0 n">6949749</td>
                              </tr>
                              <tr class="row22">
                                <td class="column0 style0 n">23</td>
                                <td class="column1"><img src="img/members/tess.jpg" width="100px"></td>
                                <td class="column2 style0 s">GAN, TESS D.</td>
                                <td class="column3 style0 s">Product Distribution Mgt.</td>
                                <td class="column4 style0 s">Jimmy</td>
                                <td class="column5 style0 n" style="font-size:0">9985933360</td>
                                <td class="column6 style0 n">10775715</td>
                              </tr>
                              <tr class="row23">
                                <td class="column0 style0 n">24</td>
                                <td class="column1"><img src="img/members/sahlee.jpg" width="100px"></td>
                                <td class="column2 style0 s">GRAIDA, SAHLEE</td>
                                <td class="column3 style0 s">Local Government Service</td>
                                <td class="column4 style0 s">Patrick</td>
                                <td class="column5 style0 n" style="font-size:0">9258321530</td>
                                <td class="column6 style0 n">10775722</td>
                              </tr>
                              <tr class="row24">
                                <td class="column0 style0 n">25</td>
                                <td class="column1"><img src="img/members/arnold.jpg" width="100px"></td>
                                <td class="column2 style0 s">HERNANDEZ, ARNOLD</td>
                                <td class="column3 style0 s">Petroleum Prod. Distribution</td>
                                <td class="column4 style0 s">Maili</td>
                                <td class="column5 style0 n" style="font-size:0">9499926574</td>
                                <td class="column6 style0 n">622563</td>
                              </tr>
                              <tr class="row25">
                                <td class="column0 style0 n">26</td>
                                <td class="column1"><img src="img/members/patrick.jpg" width="100px"></td>
                                <td class="column2 style0 s">HUNT, PATRICK</td>
                                <td class="column3 style0 s">Retired-US Army</td>
                                <td class="column4 style0 s">Sally</td>
                                <td class="column5 style0 n" style="font-size:0">9257583466</td>
                                <td class="column6 style0 n">10775711</td>
                              </tr>
                              <tr class="row26">
                                <td class="column0 style0 n">27</td>
                                <td class="column1"><img src="img/members/gael.jpg" width="100px"></td>
                                <td class="column2 style0 s">LAVINA, GAEL</td>
                                <td class="column3 style0 s">Medicine</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9177910377</td>
                                <td class="column6">&nbsp;</td>
                              </tr>
                              <tr class="row27">
                                <td class="column0 style0 n">28</td>
                                <td class="column1"><img src="img/members/romy.jpg" width="100px"></td>
                                <td class="column2 style0 s">LAZARO, ROMY</td>
                                <td class="column3 style0 s">Land Surveying</td>
                                <td class="column4 style0 s">Hermie</td>
                                <td class="column5 style0 n" style="font-size:0">9173623945</td>
                                <td class="column6 style0 n">10300283</td>
                              </tr>
                              <tr class="row28">
                                <td class="column0 style0 n">29</td>
                                <td class="column1"><img src="img/members/ken.jpg" width="100px"></td>
                                <td class="column2 style0 s">LAQUINDANUM, KEN</td>
                                <td class="column3 style0 s">Building Construction</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9256769068</td>
                                <td class="column6 style0 n">10299348</td>
                              </tr>
                              <tr class="row29">
                                <td class="column0 style0 n">30</td>
                                <td class="column1"><img src="img/members/roradel.jpg" width="100px"></td>
                                <td class="column2 style0 s">LIWANAG, RORADEL</td>
                                <td class="column3 style0 s">HR Practitioner</td>
                                <td class="column4 style0 s">Francis</td>
                                <td class="column5 style0 n" style="font-size:0">9175205161</td>
                                <td class="column6 style0 n">10778579</td>
                              </tr>
                              <tr class="row30">
                                <td class="column0 style0 n">31</td>
                                <td class="column1"><img src="img/members/tony.jpg" width="100px"></td>
                                <td class="column2 style0 s">MALLARI, TONY</td>
                                <td class="column3 style0 s">Local Government Service</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9669126975</td>
                                <td class="column6">&nbsp;</td>
                              </tr>
                              <tr class="row31">
                                <td class="column0 style0 n">32</td>
                                <td class="column1"><img src="img/members/joylyn.jpg" width="100px"></td>
                                <td class="column2 style0 s">MANIO, JOYLYN</td>
                                <td class="column3 style0 s">Restaurant Owner</td>
                                <td class="column4 style0 s">Philip</td>
                                <td class="column5 style0 n" style="font-size:0">9474735740</td>
                                <td class="column6 style0 n">10299420</td>
                              </tr>
                              <tr class="row32">
                                <td class="column0 style0 n">33</td>
                                <td class="column1"><img src="img/members/alex.jpg" width="100px"></td>
                                <td class="column2 style0 s">PAULINO, ALEX</td>
                                <td class="column3 style0 s">Poultry Equipment Supplier</td>
                                <td class="column4 style0 s">Nora</td>
                                <td class="column5 style0 n" style="font-size:0">9155423555</td>
                                <td class="column6 style0 n">9966495</td>
                              </tr>
                              <tr class="row33">
                                <td class="column0 style0 n">34</td>
                                <td class="column1"><img src="img/members/rodrigo.jpg" width="100px"></td>
                                <td class="column2 style0 s">ROXAS, RODRIGO</td>
                                <td class="column3 style0 s">Public School Mgt</td>
                                <td class="column4 style0 s">Lisa</td>
                                <td class="column5 style0 n" style="font-size:0">9663410947</td>
                                <td class="column6 style0 n">10440940</td>
                              </tr>
                              <tr class="row34">
                                <td class="column0 style0 n">35</td>
                                <td class="column1"><img src="img/members/nolan.jpg" width="100px"></td>
                                <td class="column2 style0 s">SANTOS, NOLAN</td>
                                <td class="column3 style0 s">Seafarer</td>
                                <td class="column4 style0 s">Au</td>
                                <td class="column5 style0 n" style="font-size:0">9088157041</td>
                                <td class="column6">&nbsp;</td>
                              </tr>
                              <tr class="row35">
                                <td class="column0 style0 n">36</td>
                                <td class="column1"><img src="img/members/rome.jpg" width="100px"></td>
                                <td class="column2 style0 s">SANTIAGO, ROME</td>
                                <td class="column3 style0 s">Local Government Services</td>
                                <td class="column4 style0 s">Cecilia</td>
                                <td class="column5 style0 n" style="font-size:0">9650639925</td>
                                <td class="column6">&nbsp;</td>
                              </tr>
                              <tr class="row36">
                                <td class="column0 style0 n">37</td>
                                <td class="column1"><img src="img/members/imelda.jpg" width="100px"></td>
                                <td class="column2 style0 s">SILANG, IMELDA</td>
                                <td class="column3 style0 s">Door to Door Services</td>
                                <td class="column4 style0 s">Yoshio</td>
                                <td class="column5 style0 n" style="font-size:0">9163122407</td>
                                <td class="column6 style0 n">10299451</td>
                              </tr>
                              <tr class="row37">
                                <td class="column0 style0 n">38</td>
                                <td class="column1"><img src="img/members/rod.jpg" width="100px"></td>
                                <td class="column2 style0 s">SUMULONG, ROD</td>
                                <td class="column3 style0 s">Optometry</td>
                                <td class="column4 style0 s">Zeny</td>
                                <td class="column5 style0 n" style="font-size:0">9175174789</td>
                                <td class="column6 style0 n">262277</td>
                              </tr>
                              <tr class="row38">
                                <td class="column0 style0 n">39</td>
                                <td class="column1"><img src="img/members/setti.jpg" width="100px"></td>
                                <td class="column2 style0 s">TANWANGCO, SETTI</td>
                                <td class="column3 style0 s">Tourism Industry</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n" style="font-size:0">9179510551</td>
                                <td class="column6 style0 n">10299440</td>
                              </tr>
                              <tr class="row39">
                                <td class="column0 style0 n">40</td>
                                <td class="column1"><img src="img/members/felix.jpg" width="100px"></td>
                                <td class="column2 style0 s">UMALI, FELIX JR.</td>
                                <td class="column3 style0 s">Quality Control</td>
                                <td class="column4 style0 s">Romina</td>
                                <td class="column5 style0 n" style="font-size:0">9278850163</td>
                                <td class="column6 style0 n">10775708</td>
                              </tr>
                              <tr class="row40">
                                <td class="column0 style0 n">41</td>
                                <td class="column1"><img src="img/members/manny.jpg" width="100px"></td>
                                <td class="column2 style0 s">VIARDO, MANNY</td>
                                <td class="column3 style0 s">Apartment Leasing</td>
                                <td class="column4 style0 s">Helen</td>
                                <td class="column5 style0 n" style="font-size:0">9284633575</td>
                                <td class="column6 style0 n">874534</td>
                              </tr>
                              <tr class="row41">
                                <td class="column0 style0 n">42</td>
                                <td class="column1"><img src="img/members/leonor.jpg" width="100px"></td>
                                <td class="column2 style0 s">VILLARMINO, LEONOR</td>
                                <td class="column3 style0 s">Customs Broker</td>
                                <td class="column4 style0 s">Danny</td>
                                <td class="column5 style0 n" style="font-size:0">9052858839</td>
                                <td class="column6 style0 n">10299385</td>
                              </tr>
                              <tr class="row42">
                                <td class="column0 style0 n">43</td>
                                <td class="column1"><img src="img/members/winnie.jpg" width="100px"></td>
                                <td class="column2 style0 s">CHENG, WINNIE</td>
                                <td class="column3 style0 s">Distributor-Industrial Lights</td>
                                <td class="column4">&nbsp;</td>
                                <td class="column5 style0 n " style="font-size:0">9171688960</td>
                                <td class="column6 style0 n">10271713</td>
                              </tr>
                            </tbody>

                  </table>
                </center>

               </div>
             </div>

            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>
</html>
