<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Home | Rotary Club of Malolos</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Rotary Club of Malolos" name="keywords">
    <meta content="Rotary Club of Malolos" name="description">

    <!-- Favicon -->
    <link href="img/rcmlogo.png" rel="icon">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- CSS Libraries -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/css-libraries.php') ?>

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <style>
        .ads {
            width: 100px;
            height: 100px;
        }
    </style>
</head>

<body>
    <div class="wrapper">

        <!-- Nav Bar Start -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/top-menu.php') ?>
        <!-- Nav Bar End -->


        <!-- Carousel Start -->
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                <!-- <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li> -->
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="img/real/back7.jpg" alt="Carousel Image" style="opacity:0.3">
                    <div class="carousel-caption">
                        <p class="animated fadeInRight"><img style="width:500px;height:500px;" src="img/inovated logo.png" alt="Rotary Logo" /></p>
                        <p class="animated fadeInLeft"><button onclick="beOurPartner()" style="margin-bottom: 30px;" type="button" class="btn">Be Our Partner</button></p>

                    </div>
                </div>

                <!-- <div class="carousel-item">
                      <img src="img/real/back6.jpg" alt="Carousel Image">
                        <div class="carousel-caption">
                            <p class="animated fadeInRight">Fighting Disease</p>
                          <h1 class="animated fadeInLeft">Learn how we combat disease</h1>
                         
                        </div>
                    </div> -->

                <!-- <div class="carousel-item">
                      <img src="img/real/back5.jpg" alt="Carousel Image">
                        <div class="carousel-caption">
                            <p class="animated fadeInRight">Supporting Education</p>
                          <h1 class="animated fadeInLeft">See how we improve education</h1>
                         
                        </div>
                    </div> -->
                <!-- <div class="carousel-item">
                    <img src="img/real/back6.jpg" alt="Carousel Image">
                    <div class="carousel-caption">
                      <p class="animated fadeInRight">Protecting the Environment</p>
                      <h1 class="animated fadeInLeft">Learn how we are protecting our planet</h1>
                    </div>
                  </div> -->
            </div>

            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- Carousel End -->


        <!-- Feature Start-->
        <div class="feature wow fadeInUp" data-wow-delay="0.1s">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-12">
                        <div class="feature-item">
                            <div class="feature-icon">
                                <i class="flaticon-worker"></i>
                            </div>
                            <div class="feature-text">
                                <h3>We are community-builders</h3>
                                <p>We collaborate with community leaders who want to get to work on projects that have a real, lasting impact on people’s lives.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="feature-item">
                            <div class="feature-icon">
                                <i class="flaticon-building"></i>
                            </div>
                            <div class="feature-text">
                                <h3>We are people of action</h3>
                                <p>We connect passionate people with diverse perspectives and, above all, take action to change the world.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="feature-item">
                            <div class="feature-icon">
                                <i class="flaticon-call"></i>
                            </div>
                            <div class="feature-text">
                                <h3>We are problem-solvers</h3>
                                <p>Together, we apply our professional experience and personal commitment to tackle our communities’ most persistent problems.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Feature End-->


        <!-- About Start -->
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/real/connect1.jpg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="section-header text-left">
                            <p>WE CONNECT PEOPLE</p>
                            <h2>Rotary unites more than a million people</h2>
                        </div>
                        <div class="about-text">
                            <p>
                                Together, we see a world where people unite and take action to create lasting change – across the globe, in our communities, and in ourselves.
                            </p>
                            <!--                                <a class="btn" href="">Learn More</a>-->
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="about wow fadeInUp" data-wow-delay="0.1s">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/real/connect2.jpg" alt="Image">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="section-header text-left">
                            <p>WE TRANSFORM COMMUNITIES</p>
                            <h2>We take action locally and globally</h2>
                        </div>
                        <div class="about-text">
                            <p>
                                Each day, our members pour their passion, integrity, and intelligence into completing projects that have a lasting impact. We persevere until we deliver real, lasting solutions.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-5 col-md-6">
                            <div class="about-img">
                                <img src="img/real/new/7areasoffocus.jpg" alt="Image">
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6">
                            <div class="section-header text-left">
                                <p>7 AREAS OF FOCUS</p>
                                <h2>Fulfills it's utmost duties</h2>
                            </div>
                            <div class="about-text">
                                <ol>
                                    <li>Peace Building & Conflict Prevention</li>
                                    <li>Basic Education and Literacy</li>
                                    <li>Disease Prevention and Treatment</li>
                                    <li>Water Sanitation and Hygiene</li>
                                    <li>Community Economic Development</li>
                                    <li>Maternal & Child Health</li>
                                    <li>Supporting Environment</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- About End -->


            <!-- Fact Start -->
            <div class="fact">
                <div class="container-fluid">
                    <div class="row counters">
                        <div class="col-md-6 fact-left wow slideInLeft">
                            <div class="row">
                                <div class="col-6">
                                    <div class="fact-icon">
                                        <i class="flaticon-worker"></i>
                                    </div>
                                    <div class="fact-text">
                                        <h2 data-toggle="counter-up">1.2</h2>
                                        <p>Million Members</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="fact-icon">
                                        <i class="flaticon-building"></i>
                                    </div>
                                    <div class="fact-text">
                                        <h2 data-toggle="counter-up">35000</h2>
                                        <p>Clubs</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 fact-right wow slideInRight">
                            <div class="row">
                                <div class="col-6">
                                    <div class="fact-icon">
                                        <i class="flaticon-address"></i>
                                    </div>
                                    <div class="fact-text">
                                        <h2 data-toggle="counter-up">4</h2>
                                        <p>Charity Navigator's Highest Level</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="fact-icon">
                                        <i class="flaticon-crane"></i>
                                    </div>
                                    <div class="fact-text">
                                        <h2 data-toggle="counter-up">92</h2><span>%</span>
                                        <p>Funds spent for programs awarda and operations</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fact End -->


            <!-- Service Start -->
            <!--<div class="service">
                <div class="container">
                    <div class="section-header text-center">
                        <p>THE FOUR WAY TESTS</p>
                        <h2>of the things we think, say or do</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/clubs.jpg" alt="Image">
                                    <div class="service-overlay">
                                        <p>
                                            Is it the TRUTH?
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/international.jpg" alt="Image">
                                    <div class="service-overlay">
                                        <p>
                                            Is it FAIR to all concerned?
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                       
                       
                       
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/foundation.jpg" alt="Image">
                                    <div class="service-overlay">
                                        <p>
                                            Will it be GOODWILL & BETTER FRIENDSHIPS?
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                            <div class="service-item">
                                <div class="service-img">
                                    <img src="img/healing.jpg" alt="Image">
                                    <div class="service-overlay">
                                        <p>
                                            Will it be BENEFICIAL to all concerned?
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- Service End -->


            <!-- Video Start -->
            <!-- <center><h1>Feed One Child Everyday<br/><small>In Honor of Ka MAT Caparas</small></h1></center>
            <div class="video wow fadeIn" data-wow-delay="0.1s">
                <div class="container">
                    <button type="button" class="btn-play" data-toggle="modal" data-src="img/real/Rotary Video Final.mp4" data-target="#videoModal">
                        <span></span>
                    </button>
                </div>
            </div> -->

            <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <!-- 16:9 aspect ratio -->
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item" src="" id="video" allowscriptaccess="always" allow="autoplay"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Video End -->


            <!-- Team Start -->
            <!--<div class="team">
                <div class="container">
                    <div class="section-header text-center">
                        <p>Our Members</p>
                        <h2>Meet Our RCM Members</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/team-1.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>Adam Phillips</h2>
                                    <p>CEO & Founder</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/team-2.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>Dylan Adams</h2>
                                    <p>Secretary</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/team-3.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>Jhon Doe</h2>
                                    <p>Treasurer</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                            <div class="team-item">
                                <div class="team-img">
                                    <img src="img/team-4.jpg" alt="Team Image">
                                </div>
                                <div class="team-text">
                                    <h2>Josh Dunn</h2>
                                    <p>Auditor</p>
                                </div>
                                <div class="team-social">
                                    <a class="social-tw" href=""><i class="fab fa-twitter"></i></a>
                                    <a class="social-fb" href=""><i class="fab fa-facebook-f"></i></a>
                                    <a class="social-li" href=""><i class="fab fa-linkedin-in"></i></a>
                                    <a class="social-in" href=""><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <!-- Team End -->

            <!-- Testimonial Start -->
            <div class="testimonial">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <img class="ads" src="img/ads/ads-1.jpg" alt="Testimonial">
                        </div>
                        <div class="col">
                            <img class="ads" src="img/ads/ads-2.jpg" alt="Testimonial">
                        </div>
                        <div class="col">
                            <img class="ads" src="img/ads/ads-3.jpg" alt="Testimonial">
                        </div>
                        <div class="col">
                            <img class="ads" src="img/ads/ads-3.jpg" alt="Testimonial">
                        </div>
                        <div class="col">
                            <img class="ads" src="img/ads/ads-3.jpg" alt="Testimonial">
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-12">
                            <div class="testimonial-slider-nav">
                                <div class="slider-nav"><img src="img/ads/ads-1.jpg" alt="Testimonial"></div>
                                <div class="slider-nav"><img src="img/ads/ads-3.jpg" alt="Testimonial"></div>
                                <div class="slider-nav"><img src="img/ads/ads-4.jpg" alt="Testimonial"></div>


                                <div class="slider-nav"><img src="img/ads/ads-1.jpg" alt="Testimonial"></div>
                                <div class="slider-nav"><img src="img/ads/ads-3.jpg" alt="Testimonial"></div>
                                <div class="slider-nav"><img src="img/ads/ads-4.jpg" alt="Testimonial"></div>

                            </div>
                        </div>
                    </div> -->
                    <!-- <div class="row">
                        <div class="col-12">
                            <div class="testimonial-slider">
                                <div class="slider-item">
                                    <h3>SMARTCOM</h3>

                                    <p>Your CCTV and Computer Providers. For more info visit our fb page <a href="https://www.facebook.com/SmartcomSolution">here</a></p>
                                </div>



                                <div class="slider-item">
                                    <h3>1Tech Providers</h3>

                                    <p>Your POS Solutions. Visit our website <a href="https://www.1teqproviders.com.ph/">here</a></p>
                                </div>


                                <div class="slider-item">
                                    <h3>Augustine's Events Place and Catering Service</h3>


                                    <p><a href="https://www.facebook.com/pages/Augustine's%20Events%20Place%20&%20Catering%20Services/196011521352531/">FB PAGE</a></p>
                                </div>


                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- Testimonial End -->


            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/footer.php') ?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/includes/common/js.php') ?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            function beOurPartner() {
                window.location.href = "/contact-us";
            }
        </script>
</body>

</html>