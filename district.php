<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
      <title>About RCM | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">

        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

        <!-- CSS Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php')?>

        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>

  <body>
    <div class="wrapper">

      <!-- Nav Bar Start -->
      <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
      <!-- Nav Bar End -->
            
            
            <!-- Page Header Start -->
            <div class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2>District 3770</h2>
                        </div>
                        <div class="col-12">
                            <a href="">Home</a>
                          <a href="">About District 3770</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page Header End -->

                      <div class="about wow fadeInUp" data-wow-delay="0.1s">
                          <div class="container">
                              <div class="row align-items-center">
                                  <div class="col-lg-12 col-md-12">
                                      <div class="section-header text-left">
                                          <p>RPDG FRANCISCO “Jun” ANIAG, JR.<br/>
                                          Past Chairman, Philippine College of Rotary Governors<br/>
                                          Congressman, 1 st District of Bulacan, 8 th Congress of the Philippines</p>
                                        <h2>HISTORY OF DISTRICT 3770</h2>
                                      </div>
                                      <div class="about-text">
                                          <p>
                                          The Rotary Clubs of what is now RI District 3770 used to be part of District 380 which then
                                          comprised of Rotary Clubs north of the Pasig River.
                                          </p>
                                          <p>
                                          The oldest club in the district is the Rotary Club of Cabanatuan which was sponsored by the
                                          Rotary Club of Tarlac in 1948. In 1949, the Rotary Club of Malolos was organized and sponsored
                                          by the Rotary Club of Manila.
                                          </p>

                                          <p>
                                          The organization of the Rotary Club of Malolos was unique in the sense that Rotary was not
                                          brought but was asked for. PP Francisco “Frank” Aniag, Sr., who was not a Rotarian then, and
                                          who later was conferred the title, “The Pioneer Rotarian of Bulacan” conceived the idea of
                                          forming a Rotary Club in the province. He broached the idea to then Provincial Treasurer
                                          Vicente “Titong” Avila who was then a former Dagupan Rotarian, then Provincial Fiscal Feliciano
                                          “Fely” Torres and Teofilo “Magmamani” Sauco and together, they contacted and invited
                                          prominent personalities of Bulacan and explained Rotary. When the group was already
                                          organized and was meeting regularly, PP Frank contacted then DG Emy Javier, then PRIVP Gil
                                          Puyat (who later became Senate President) and then Pres. Eduardo Romualdez of the Rotary
                                          Club of Manila for their sponsorship. Malolos became the 15 th Rotary club to be organized in the
                                          Philippines.
                                          </p>

                                          <p>
                                          The history of the district would not be complete without mention of the pioneering efforts of
                                          the Rotary Club of Malolos, particularly PP Maximo “Maxie” Valenzuela to expand Rotary in this
                                          part of the Philippines. It organized and sponsored the Rotary Club of Caloocan (D3800), with
                                          PRIP Mat Caparas, who is a Maloleno and Delegate of the 1 st District of Bulacan to the 1973
                                          Constitutional Convention, as one of the charter members. This was followed by the
                                          organization of the Rotary Clubs of Baliuag, Valenzuela (D3800), Sta. Maria, Meycauayan,
                                          Bocaue, Plaridel, Balagtas, Hagonoy, Bulacan, Barasoain and Malolos Hiyas.
                                          </p>

                                          <p>
                                          PDG Marianito “Ito” Tinio of the Rotary Club of Cabanatuan was the first governor to come
                                          from the area. He served as district governor of District 48 in RY 1956-57. The Philippines was
                                          redistricted into two districts in RY 1964-65. PRIP MAT Caparas served as the first governor of
                                          District 380. PRID Sabino “Benny” Santos of the Rotary Club of Malolos followed in RY 1970-71
                                          and he rose to become one of only 8 Filipinos so far who became Directors of Rotary
                                          International. He was revered in the whole Rotary world and pioneered the Polio Plus Project of
                                          the Rotary Foundation.
                                          </p>

                                          <p>
                                          PDG Filemon “Fil” Mendoza of the Rotary Club of Sta. Maria in RY 1984-85 became district
                                          governor of District 380 when western Luzon provinces, from Pampanga to Ilocos were
                                          separated from the district to form District 379. The Philippines became 6 districts then.
                                          Rotary started to grow very fast in the Philippines. In RY 1987-88, District 380 was divided into 3
                                          districts, D377, comprising of Rotary Clubs in Eastern Northern Luzon, D378 in Quezon City and
                                          D380, in Metro Manila cities north of the Pasig River.
                                          </p>

                                          <p>
                                          District 377 initially consisted of 29 Rotary clubs with 19 Rotary clubs from Bulacan and 10
                                          outside of Bulacan. One of the requirements of Rotary International for forming a new district is
                                          it must have a suitable convention site. Our district, up to now does not have one that will meet
                                          Rotary standards and for this reason, out of 25 Discons that the district had, we only held 2
                                          discons within our territory, while 23 were held outside, with 17 held in Baguio and 6 in other
                                          cities still outside the territory. It is a common practice even in Rotary International that
                                          elective positions are rotated among the component areas. But because of the disparity
                                          between the number of Bulacan clubs and those outside, it was decided in the 1986 Discon,
                                          that no rotation agreement will be put in effect, in order to give more chance for clubs outside
                                          of Bulacan to have a district governor.
                                          </p>

                                          <p>
                                          PDG Francisco “Jun” Aniag, Jr., now a member of Rotary Club of Barasoain served as the first
                                          governor of District 377. His selection and that of the 2 governors that followed were done by
                                          election of all the club electors during the Discon. Thereafter, District 3770 selection of
                                          governor through the nominating committee system. Under this method, the selection made
                                          by the nominating committee can be challenged by a club whose suggested candidate was not
                                          chosen, concurred by at least 5 other Rotary clubs within 15 days after announcement of the
                                          selection. Election then is done through ballot by mail. To the credit of our district and as a
                                          profound display of unity and solidarity, all selections save for two that were made by the
                                          nominating committees have been unchallenged. This is in stark contrast to other districts in
                                          the Philippines where most of the nominating committee selections were challenged and the
                                          challenger usually wins. In RY 1991-92, the number District 377 was changed to District 3770.
                                          </p>

                                          <p>

                                          <pre>
          The district governors coming from Rotary Clubs belonging now to District 3770 are:

          District 48
          1956-57 PDG Mariano “Ito” Tinio+ Rotary Club of Cabanatuan

          District 380
          1970-71 PRID Sabino “Benny” Santos+ Rotary Club of Malolos
          1984-85 PDG Filemon “Fil” Mendoza + Rotary Club of Sta. Maria

          District 377
          1987-88 PDG Francisco “Jun” Aniag, Jr. + Rotary Club of Barasoain
          1988-89 PDG Rafael “Paeng” Co Seng + Rotary Club of Santiago
          1989-90 PDG Vicente “Tito” Enriquez Rotary Club of Bulacan
          1990-91 PDG Edmundo “Eddie” Castelo + Rotary Club of Cabanatuan


          District 3770
          1991-92 PDG Venustiano “Venus” Roxas+ Rotary Club of Sta. Maria
          1992-93 PDG Pacifico “Boy” Aniag Rotary Club of Malolos
          1993-94 PDG Ramelo “Ramy” Ramirez Rotary Club of Tuguegarao
          1994-95 PDG Felix “Felix” Domigpe Rotary Club of Metro Meycauayan
          1995-96 PDG Ponciano “Pons” Hernandez +
          1996-97 PDG Romulo “Boy” Valle Rotary Club of Cauayan
          1997-98 PDG Manuel “Manny” Punzalan Rotary Club of Calumpit
          1998-99 PDG Leonardo “Narding” Dela Cruz+ Rotary Club of Sta. Maria
          1999-00 PDG Rodolfo “Rody” Zamora Rotary Club of San Jose del Monte
          2000-01 PDG Frederic “Eric” Wycoco Rotary Club of Cabanatuan
          2001-02 PDG Diosab “Diosab” Formilleza +
          2002-03 PDG Efren “Efren” Martinez Rotary Club of Sta. Maria
          2003-04 PDG Hilarion “Larry” Aquino + Rotary Club of Tuguegarao
          2004-05 PDG Severino “Vino” Abela + Rotary Club of San Jose del Monte
          2005-06 PDG Mauro “Ogie” Villamar + Rotary Club of Marilao
          2006-07 PDG Gilford “Ted” Briones Rotary Club of Cabanatuan West
          2007-08 PDG Rogenio “Rogie” Fragante Rotary Club of Tuguegarao Rainbow
          2008-09 PDG Miguelito “Lito” Jose Rotary Club of Sta. Maria
          2009-10 PDG Ricardo “Ding” San Diego Rotary Club of Plaridel
          2010-11 PDG Melvin “Melvin” Tiongson Rotary Club of Vizcaya
          2011-12 PDG Socorro “Corina” Bautista Rotary Club of Malolos Hiyas
          2012-13 PDG Yolanda “Yolly” Wycoco Rotary Club of Cabanatuan
          2013-14 PDG Benigno “Pichoy” Ramirez Rotary Club of Tugegarao
          2014-15 PDG Ma. Nonette “Non” Tiam Rotary Club of Bayombong
          2015-16 PDG Manuel “Manny” Sy Ping Rotary Club of Meycauayan Uptown
          2016-17 PDG George “George” Bunuan Rotary Club of Malolos
          2017-18 PDG Narciso “Siso” S. Salunat Rotary Club of Vizcaya
          2018-19 PDG Celso C. Cruz Rotary Club of Guiguinto Suburbs
          2019-20 PDG Bienvenido “Jon” Alonzo Rotary Club Santa Maria
          2020-21 IPDG Gil Dindo O. Berino Rotary Club of Cabanatuan North
          2021-22 DGOV Arturo “Art” Que Rotary Club of Tuguegarao Citadel
          2022-23 DGE Marcelino “Noli” Garcia Rotary Club of Malolos Barasoain
          2023-24 DGN Rpdolfo DC “Rudy” Enriquez Rotary Club of Meycauayan East
          +deceased
                                          </pre>
                                          </p>

                                          <p>
                                          In the Philippines, PDGs Jun and Boy Aniag are the second of 3 siblings who became district
                                          governors and PDG and DGE Eric and Yolly Wycoco are the third of 3 husband and wife to
                                          become district governors.
                                          </p>

                                          <p>
                                          The district was a little behind in finally accepting women to Rotary. Still, a good number of
                                          clubs remain bastions of male exclusiveness. Realizing however the unique power and
                                          capabilities of women in pursuing the goal of service gave rise to the formation of all women
                                          clubs, of which there now 6 in the district, in such areas where more effective partnership and
                                          cooperation could be pursued by gender defined clubs. The first lady governor, DG Corina
                                          Bautista, who rose to become district governor only in her 8 th year as a Rotarian, is a member of
                                          an all women club, the Rotary Club of Malolos Hiyas. Two years after her governorship, she was
                                          elected Chair of the PCRG, the first lady governor to head the organization.
                                          </p>

                                          <p>
                                          Four PDGs have been designated as RI President’s Personal Representative to various district
                                          conferences in the world. They are PRID Benny Santos to a number of districts, PDG Jun Aniag
                                          to District 3850, PDG Ogie Villamar to District 3870 and PDG Corina Tengco Bautista to District
                                          3870.
                                          </p>

                                          <p>
                                          The District 3770 Council of Governors, composed of the incumbent governor, the past
                                          governors, DGEs and DGNs of the district supports, assists and provides counsel to the
                                          incumbent district governors and Rotary clubs of the district. It is a member-council of the
                                          Philippine College of Rotary Governors. The following governors from District 3770 became
                                          Chairs of the PCRG:
                                          </p>

                                          <p>
                                          1982-83 PRID Benny Santos<br/>
                                          2007-08 PDG Jun Aniag<br/>
                                          2013-14 PDG Corina Tengco Bautista<br/>
                                          2015-16 PDG Benigno Emilio “Pichoy” P. Ramirez
                                          </p>

                                          <p>
                                          The following served as Chairmen of the District 3770 Council of Governors:
                                          </p>

                                          <p>
                                          1995-97 PDG Venus Roxas 1997-99 PDG Boy Aniag<br/>
                                          1999-01 PDG Tito Enriquez 2001-03 PDG Jun Aniag<br/>
                                          2003-05 PDG Manny Punzalan 2005-07 PDG Efren Martinez<br/>
                                          2007-09 PDG Ogie Villamar 2009-11 PDG Felix Domigpe<br/>
                                          2011-13 PDG Boy Valle 2013-18 PDG Pichoy Ramirez<br/>
                                          2019-21 PDG George B. Bunuan 2021-23
                                          </p>

                                          <p>
                                          District 3770 today is a very strong, vibrant and steadfast district pursuing vigorously the object
                                          and mission of Rotary in this side of the world. It is composed of Rotarians who are committed
                                          to fellowship and service which is a result of an unbroken, consistent and well planned
                                          programs and activities advanced by every district governor that adhere strictly to the tenets
                                          and prescriptions of Rotary International.
                                          </p>


                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>







            <!-- About Start -->
            <div class="about wow fadeInUp" data-wow-delay="0.1s">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-md-6">
                              <div class="about-img">
                                  <img src="img/real/new/districtlogo.png" alt="Image">
                              </div>
                              <br/><br/>
                            <div class="about-img">
                                <img src="img/real/district.jpg" alt="Image">
                            </div>

                        </div>

                        <div class="col-lg-7 col-md-6">
                            <div class="section-header text-left">
                                <p>Rotary Club </p>
                              <h2>DISTRICT 3770</h2>
                            </div>
                            <div class="about-text">
                                <p>
                                  The Rotary International District 3770 of the Philippines used to be part of District 3800 which then comprised the Rotary Clubs north of the Pasig River. During 1987-88, District 3800 was divided into 3 districts, and District 3770 was composed with the Rotary Clubs in Eastern Northern Luzon.
                                </p>
                                <center><strong>Rotary Clubs of District 3770</strong></center><br/><br/>
                                <div class ="row">

                                   <div class="col-lg-4 col-md-4">
                                     <li>Angat</li>
                                     <li>Barasoain</li>
                                     <li>Bayombong</li>
                                     <li>Bocaue</li>
                                     <li>Bulacan</li>
                                     <li>Cabanatuan</li>
                                     <li>Cabanatuan East</li>
                                     <li>Cabanatuan West</li>
                                     <li>Cabiao</li>
                                     <li>Calumpit</li>
                                     <li>Cauayan</li>
                                     <li>Gapan</li>
                                     <li>Guiguinto</li>
                                     <li>Ilagan</li>
                                     <li>Lagawe</li>
                                     <li>Malolos</li>
                                     <li>Malolos Hiyas</li>
                                     <li>Marilao</li>
                                     <li>Metro Baliuag</li>
                                       <li>Metro Cauayan</li>
                                       <li>Metro Guimba</li>
                                       <li>Metro Tuguegarao</li>
                                       <li>Metropolitan Malolos</li>
                                   </div>

                                   <div class="col-lg-4 col-md-4">

                                      <li>Meycauayan</li>
                                      <li>Meycauayan Central</li>
                                      <li>Meycauayan East</li>
                                      <li>Meycauayan Gold</li>
                                      <li>Meycauayan Metro</li>
                                      <li>Meycauayan Northeast</li>
                                      <li>Meycauayan Uptown</li>
                                      <li>Midtown Santiago</li>
                                      <li>Mu&ntilde;os Centro</li>
                                      <li>Mutya ng Sta. Maria Bulacan</li>
                                      <li>Norzagaray</li>
                                      <li>Pandi</li>
                                      <li>Paombong</li>
                                      <li>Pe&ntilde;aranda</li>
                                      <li>Plaridel</li>
                                        <li>Plaridel Kristal</li>
                                          <li>Plaridel South</li>
                                          <li>Pulilan</li>
                                          <li>Roxas</li>
                                          <li>San Ildefonso</li>
                                          <li>San Jose Del Monte</li>

                                    </div>



                                    <div class="col-lg-4 col-md-4">

                                        <li>Bulacan</li>
                                          <li>San Jose Del Monte Central</li>
                                          <li>Bulacan</li>
                                        <li>San Miguel De Mayumo</li>
                                        <li>San Miguel De Mayumo East</li>
                                        <li>San Miguel De Mayumo North</li>
                                        <li>San Rafael</li>
                                        <li>Santa Marcela</li>
                                        <li>Apayao</li>
                                        <li>Santa Maria</li>
                                        <li>Bulacan</li>
                                        <li>Santiago</li>
                                        <li>Tabuk</li>
                                        <li>Talavera</li>
                                        <li>Tuguegarao Citadel</li>
                                        <li>Tuguegarao Gems</li>
                                        <li>Tuguegarao Rainbow</li>
                                        <li>Vizcaya</li>
                                        <li>Vizcaya South</li>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About End -->
            
            
      <!-- Fact Start -->
      <div class="fact">
        <div class="container-fluid">
          <div class="row counters">
            <div class="col-md-6 fact-left wow slideInLeft">
              <div class="row">
                <div class="col-6">
                  <div class="fact-icon">
                    <i class="flaticon-worker"></i>
                  </div>
                  <div class="fact-text">
                    <h2 data-toggle="counter-up">1.2</h2>
                    <p>Million Members</p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="fact-icon">
                    <i class="flaticon-building"></i>
                  </div>
                  <div class="fact-text">
                    <h2 data-toggle="counter-up">35000</h2>
                    <p>Clubs</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 fact-right wow slideInRight">
              <div class="row">
                <div class="col-6">
                  <div class="fact-icon">
                    <i class="flaticon-address"></i>
                  </div>
                  <div class="fact-text">
                    <h2 data-toggle="counter-up">4</h2>
                    <p>Charity Navigator's Highest Level</p>
                  </div>
                </div>
                <div class="col-6">
                  <div class="fact-icon">
                    <i class="flaticon-crane"></i>
                  </div>
                  <div class="fact-text">
                    <h2 data-toggle="counter-up">92</h2><span>percent</span>
                    <p>Funds spent for programs awarda and operations</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Fact End -->
            
            <!-- FAQs Start -->
<!--            <div class="faqs">-->
<!--                <div class="container">-->
<!--                    <div class="section-header text-center">-->
<!--                        <p>Frequently Asked Question</p>-->
<!--                        <h2>You May Ask</h2>-->
<!--                    </div>-->
<!--                    <div class="row">-->
<!--                        <div class="col-md-6">-->
<!--                            <div id="accordion-1">-->
<!--                                <div class="card wow fadeInLeft" data-wow-delay="0.1s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseOne">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseOne" class="collapse" data-parent="#accordion-1">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="card wow fadeInLeft" data-wow-delay="0.2s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseTwo">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseTwo" class="collapse" data-parent="#accordion-1">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="card wow fadeInLeft" data-wow-delay="0.3s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseThree">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseThree" class="collapse" data-parent="#accordion-1">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="card wow fadeInLeft" data-wow-delay="0.4s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseFour">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseFour" class="collapse" data-parent="#accordion-1">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="card wow fadeInLeft" data-wow-delay="0.5s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseFive">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseFive" class="collapse" data-parent="#accordion-1">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <div id="accordion-2">-->
<!--                                <div class="card wow fadeInRight" data-wow-delay="0.1s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseSix">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseSix" class="collapse" data-parent="#accordion-2">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="card wow fadeInRight" data-wow-delay="0.2s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseSeven">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseSeven" class="collapse" data-parent="#accordion-2">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="card wow fadeInRight" data-wow-delay="0.3s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseEight">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseEight" class="collapse" data-parent="#accordion-2">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="card wow fadeInRight" data-wow-delay="0.4s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseNine">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseNine" class="collapse" data-parent="#accordion-2">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="card wow fadeInRight" data-wow-delay="0.5s">-->
<!--                                    <div class="card-header">-->
<!--                                        <a class="card-link collapsed" data-toggle="collapse" href="#collapseTen">-->
<!--                                            Lorem ipsum dolor sit amet?-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div id="collapseTen" class="collapse" data-parent="#accordion-2">-->
<!--                                        <div class="card-body">-->
<!--                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non.-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <!-- FAQs End -->


            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>
</html>
