<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Grants | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

            <!-- Single Post Start-->
            <div class="single">
                <div class="container">
                    <h2>Global/Matching Grant Projects</h2>
                    <div class="row">
                        <div class="accordion" id="accordionExample">

                         <!-- GLOBAL GRANT # GG1872989 (USD 52,585): Construction of Welding Training Center and Donation of Welding Equipment, Tools and Materials to the City of Malolos -->
                         <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading1">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse1" aria-expanded="false" aria-controls="flush-collapse1">
                                <?php renderTable("GLOBAL GRANT # GG1872989 (USD 52,585): Construction of Welding Training Center and Donation of Welding Equipment, Tools and Materials to the City of Malolos
                                        ","2019","Rotary Club of Jincheon Bonghwa 
                                        and Rotary Club of Tongyoung-Hanryeo, and RI Districts 3770, 3740,
                                        3590 and 3502
                                        ","-"); 
                                ?>   
                            </button>
                                </h2>
                                <div id="flush-collapse1" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading1" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(26, 'global-grant-construction-welding-training-center', 'ggcwtc');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                             <!-- GLOBAL GRANT # GG1744906 (USD 64,888.00): Construction of Toilets and Drinking Stations, and Donation of Science Laboratory, Kitchen and Other School Equipment to Five (5) Schools in the Island Barangays of the City of Malolos.  -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading11">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse11" aria-expanded="true" aria-expanded="false" aria-controls="flush-collapse11">
                                <?php renderTable("GLOBAL GRANT # GG1744906 (USD 64,888.00): Construction of Toilets and Drinking Stations, and Donation of Science Laboratory, Kitchen and Other School Equipment to Five (5) Schools in the Island Barangays of the City of Malolos.","2018","-","-"); ?>   
                            </button>
                                </h2>
                                <div id="flush-collapse11" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading11" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(45, 'global-grant-construction-toilets-drinking-stations', 'ggctds');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                           
                            <!-- GLOBAL GRANT # GG1525916 (USD 61,500): Donation of Kitchen Laboratory Equipment to Bulacan Polytechnic College. -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading2">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse2" aria-expanded="false" aria-controls="flush-collapse2">
                                
                                <?php renderTable("GLOBAL GRANT # GG1525916 (USD 61,500): Donation of Kitchen Laboratory Equipment to Bulacan Polytechnic College.
                                        ","2016","Rotary Club of Jincheon Bonghwa 
                                        and RI Districts 3770 and 3740","-"); 
                                ?>    
                              </button>
                                </h2>
                                <div id="flush-collapse2" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading2" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(15, 'global-grant-bpc', 'ggbpc');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                             <!-- DONATION OF BIRTHING FACILITIES to Hagonoy District Hospital In Partnership with Rotary Club of Jincheon Bonghwa and RI Districts 3770 and 3740-->
                             <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading3">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse3" aria-expanded="false" aria-controls="flush-collapse3">
                                <?php renderTable("DONATION OF BIRTHING FACILITIES to Hagonoy District Hospital","2016","Rotary Club of Jincheon Bonghwa 
                                        and RI Districts 3770 and 3740
                                        ","-"); 
                                ?>     
                            </button>
                                </h2>
                                <div id="flush-collapse3" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading3" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(14, 'global-grant-birthing-facility-hagonoy', 'ggbfh');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MATCHING GRANT PROJECT (USD 60,000) Donation of Science Laboratory Equipment To Barasoain Memmorial Elementary School In Partnership with Rotary Club of Jincheon Bonghwa, D3740 and Rotary Club of Makati Bonifacio
-->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading4">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse4" aria-expanded="false" aria-controls="flush-collapse4">
                               
                                <?php renderTable("MATCHING GRANT PROJECT (USD 60,000) Donation of Science Laboratory Equipment To Barasoain Memmorial Elementary School","2011","Rotary Club of Jincheon Bonghwa, D3740 and Rotary Club of Makati Bonifacio
                                        ","-"); 
                                ?>  
                            </button>    
                            </h2>
                                <div id="flush-collapse4" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading4" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(11, 'global-grant-science-equipment', 'ggse');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Post End-->
            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Grants";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
