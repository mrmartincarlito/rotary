<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Water, Sanitation & Hygiene | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

            <!-- Single Post Start-->
            <div class="single">
                <div class="container">
                    <h2>Water, Sanitation and Hygiene</h2>
                    <div class="row">
                        <div class="accordion" id="accordionExample">
                            <!-- GLOBAL GRANT # GG1744906 (USD 64,888.00): Construction of Toilets and Drinking Stations, and Donation of Science Laboratory, Kitchen and Other School Equipment to Five (5) Schools in the Island Barangays of the City of Malolos.  -->
                            <!-- <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading1">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse1" aria-expanded="false" aria-controls="flush-collapse1">
                                <?php renderTable("GLOBAL GRANT # GG1744906 (USD 64,888.00): Construction of Toilets and Drinking Stations, and Donation of Science Laboratory, Kitchen and Other School Equipment to Five (5) Schools in the Island Barangays of the City of Malolos.
                                ","December 21, 2018","-","-"); ?>                                
                                </button>
                                </h2>
                                <div id="flush-collapse1" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading1" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(45, 'global-grant-construction-toilets-drinking-stations', 'ggctds');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- Global Handwashing Day - Distribution of Hygiene Kit to San Pablo Elementary, City of Malolos in partnership with Inner Wheel Club of Malolos and Inner Wheel Club of Malolos North.  -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingSeven">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSeven" aria-expanded="false" aria-controls="flush-collapseSeven">
                                <?php renderTable("Hand Washing Project","August 25, 2022","Vice Mayor's Office and IWC Malolos","San Pablo Elementary School (305 Students)"); ?>    
                                </button>
                                </h2>
                                <div id="flush-collapseSeven" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingSeven" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(12, 'global-handwashing-day', 'ghd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Post End-->
            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Water, Sanitation and Hygiene";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
