<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Environment Protection | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

             <!-- Single Post Start-->
             <div class="single">
                <div class="container">
                    <h2>Environment Protection</h2>
                    <div class="row">
                        <div class="accordion" id="accordionExample">
                            <!-- Mangrove Planting at Bgy. Babatnin, City of Malolos in partnership with United Architects of the Philippines, Bulacan Chapter -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingTwelve">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwelve" aria-expanded="false" aria-controls="flush-collapseTwelve">
                                <?php renderTable("Bakawan Planting at Brgy. Babatnin","July 30, 2022","Architech Association of the Philippines, Bulacan Chapter","Coastal Area of Malolos"); ?>    
                            </button>
                                </h2>
                                <div id="flush-collapseTwelve" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingTwelve" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(26, 'mangrove-planting', 'mp');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Needs Assessment meeting 
                            with Green Antz Builders Inc. & Bgy. Capt. Belty Bartolome of Bgy. Pamarawan regarding handling of waste plastics.  -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading14">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse14" aria-expanded="false" aria-controls="flush-collapse14">
                                Needs Assessment meeting with Green Antz Builders Inc. & Bgy. Capt. Belty Bartolome of Bgy. Pamarawan regarding handling of waste plastics.
                                </button>
                                </h2>
                                <div id="flush-collapse14" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading14" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(18, 'needs-assesment-meeting', 'nam');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Post End-->
            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Environment Protection";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
