<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Disease Control and Prevention | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

             <!-- Single Post Start-->
             <div class="single">
                <div class="container">
                    <h2>Disease Control and Prevention</h2>
                    <div class="row">
                        <div class="accordion" id="accordionExample">
                            <!-- Blood Donation Program in cooperation with the Office of the Vice Mayor, City of Malolos -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingEleven">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseEleven" aria-expanded="false" aria-controls="flush-collapseEleven">
                                <?php renderTable("Blood Donation Drive","August 16, 2022","Vice Mayor's Office","Philippine Red Cross (30 Donors)"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseEleven" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingEleven" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(22, 'blood-donation', 'bd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Post End-->
            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Disease Control and Prevention";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
