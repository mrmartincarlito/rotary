<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Other Community Service | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

            <!-- Single Post Start-->
            <div class="single">
                <div class="container">
                    <h2>Other Community Services</h2>
                    <div class="row">
                        <div class="accordion" id="accordionExample">
                            <!-- GIFT GIVING - STATIONARY WITH D3740 GOVERNOR NAM JAE-HO AND ASSISTANT GOVERNORS AT PITPITAN ELEMENTARY SCHOOL -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="true" aria-controls="flush-collapseThree">
                                
                                <?php renderTable("Gift Giving - Stationary with D3740 Governor Nam Jae-Ho and Assistant Governors at Pitpitan Elementary School","April 3, 2023","IWC Malolos","100 Students"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseThree" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(8, 'gift-giving-pitpitan-school', 'ggd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- GIFT GIVING – GROCERIES AND RICE WITH D3740 GOVERNOR NAM JAE-HO AND ASSISTANT GOVERNORS AT NIA ROAD, PITPITAN-->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingThree3">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree3" aria-expanded="true" aria-controls="flush-collapseThree3">
                                
                                <?php renderTable("Gift Giving - Groceries with D3740 Governor Nam Jae-Ho and Assistant Governors at Nia Road, Pitpitan","April 3, 2023","IWC Malolos","103 Families"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseThree3" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingThree3" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(6, 'gift-giving-groceries', 'ggd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- PAMASKONG HATID SA MGA BATANG MAY KAPANSANAN -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingFive">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                                <?php renderTable("Gift Giving Project - Akapin Foundation","December 19, 2022","IWC Malolos","70 PWD Children"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseFive" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(22, 'pamaskong-hatid-kapansanan', 'phk');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- School Supplies - Turn Over (ALS - Malolos Market) -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingNine">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine" aria-expanded="false" aria-controls="flush-collapseNine">
                                <?php renderTable("School Supplies - Turn Over (ALS - Malolos Market)","September 9, 2022","IWC Malolos and IWC Malolos North","25"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseNine" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingNine" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(2, 'school-supplies-turnover-als-malolos-market', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- School Supplies - Turn Over (ALS - City Jail) -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingNine9">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine9" aria-expanded="false" aria-controls="flush-collapseNine9">
                                <?php renderTable("School Supplies - Turn Over (ALS - City Jail)","September 9, 2022","IWC Malolos and IWC Malolos North","30"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseNine9" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingNine9" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(3, 'school-supplies-turnover-als-city-jail', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- School Supplies - Turn Over (ALS - Tanglaw Pag-asa) -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingNine99">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine99" aria-expanded="false" aria-controls="flush-collapseNine99">
                                <?php renderTable("School Supplies - Turn Over (ALS - Tanglaw Pag-asa)","September 9, 2022","IWC Malolos and IWC Malolos North","20"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseNine99" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingNine99" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(4, 'school-supplies-turnover-als-tanglaw-pagasa', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- School Supplies - Turn Over (ALS - Provincial Jail) -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingNine999">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseNine999" aria-expanded="false" aria-controls="flush-collapseNine999">
                                <?php renderTable("School Supplies - Turn Over (ALS - Provincial Jail)","September 9, 2022","IWC Malolos and IWC Malolos North","35"); ?>    

                                </button>
                                </h2>
                                <div id="flush-collapseNine999" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingNine999" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(3, 'school-supplies-turnover-als-provincial-jail', 'sst');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Anti-Rabies Vaccination  -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading13">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse13" aria-expanded="false" aria-controls="flush-collapse13">
                                <?php renderTable("Free Capon and Anti-Rabbies for Dogs and Cats","July 8, 2022","Vice Mayor's Office","70"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapse13" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading13" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(16, 'libreng-kapon', 'lk');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Post End-->
            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Other Community Service";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
