<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Livelihood & Community Dev't. | Rotary Club of Malolos</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="Construction Company Website Template" name="keywords">
        <meta content="Construction Company Website Template" name="description">
        <!-- Favicon -->
         <link href="img/real/new/logo.jpg" rel="icon">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;
        800;900&display=swap" rel="stylesheet">
        <?php
        // CSS Libraries
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/css-libraries.php');
        // Render Image Function 
        include($_SERVER['DOCUMENT_ROOT'].'/includes/common/functions.php');
        ?>
        <!-- Template Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="wrapper">

            <!-- Nav Bar Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/top-menu.php')?>
            <!-- Nav Bar End -->

            <!-- Page Header Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/page-header.php')?>
            <!-- Page Header End -->

            <!-- Single Post Start-->
            <div class="single">
                <div class="container">
                    <h2>Livelihood and Community Devt.</h2>
                    <div class="row">
                        <div class="accordion" id="accordionExample">
                            <!-- GLOBAL GRANT # GG1872989 (USD 52,585): Construction of Welding Training Center and Donation of Welding Equipment, Tools and Materials to the City of Malolos -->
                            <!-- <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading1">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse1" aria-expanded="false" aria-controls="flush-collapse1">
                                <?php renderTable("GLOBAL GRANT # GG1872989 (USD 52,585): Construction of Welding Training Center and Donation of Welding Equipment, Tools and Materials to the City of Malolos
                                        ","2019","Rotary Club of Jincheon Bonghwa 
                                        and Rotary Club of Tongyoung-Hanryeo, and RI Districts 3770, 3740,
                                        3590 and 3502
                                        ","-"); 
                                ?> 
                                </button>
                                </h2>
                                <div id="flush-collapse1" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading1" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(26, 'global-grant-construction-welding-training-center', 'ggcwtc');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- GLOBAL GRANT # GG1525916 (USD 61,500): Donation of Kitchen Laboratory Equipment to Bulacan Polytechnic College. -->
                            <!-- <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-heading2">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse2" aria-expanded="false" aria-controls="flush-collapse2">
                                <?php renderTable("GLOBAL GRANT # GG1525916 (USD 61,500): Donation of Kitchen Laboratory Equipment to Bulacan Polytechnic College.
                                        ","2016","Rotary Club of Jincheon Bonghwa 
                                        and RI Districts 3770 and 3740","-"); 
                                ?>     
                                </button>
                                </h2>
                                <div id="flush-collapse2" class="accordion-collapse collapse" 
                                aria-labelledby="flush-heading2" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(15, 'global-grant-bpc', 'ggbpc');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- Animal Dispersion Program of the Provincial Gov't of Bulacan where RC Malolos announced its project of distributing disinfectants agianst African Swine Fever Virus (ASFV) for backyard pig farms in Bulacan. -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingFour">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                                <?php renderTable("(GGP) Safe and Effective Disinfection of Small Pig Farms Against African Swine Fever Virus (ASFV)","March 14, 2023","","325 Small Pig Farm"); ?>  
                                </button>
                                </h2>
                                <div id="flush-collapseFour" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(16, 'swine-fever-disinfection', 'sfd');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Livelihood Training: Buchi & Chicken Nuggets Making   -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingEight">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseEight" aria-expanded="false" aria-controls="flush-collapseEight">
                                <?php renderTable("Livelihood Seminar - Buchi and Chicken Nuggets Making","September 17, 2022","IWC Malolos and IWC Malolos North","26"); ?>
                                </button>
                                </h2>
                                <div id="flush-collapseEight" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingEight" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(5, 'livelihood-training-buchi', 'lt');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Livelihood Training: Dishwashing Liquid & Fabric Conditioner   -->
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingEight8">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseEight8" aria-expanded="false" aria-controls="flush-collapseEight8">
                                <?php renderTable("Livelihood Seminar - Powder Detergent Dishwashing Liquid and Fabric Conditioner","September 17, 2022","IWC Malolos and IWC Malolos North","20"); ?>

                                </button>
                                </h2>
                                <div id="flush-collapseEight8" class="accordion-collapse collapse" 
                                aria-labelledby="flush-headingEight8" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                            <div class="container">
                                                <div class="row">
                                                    <?php
                                                    renderImage(6, 'livelihood-training-detergent', 'lt');
                                                    ?>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Single Post End-->
            
            <!-- Footer Start -->
            <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/footer.php')?>
            <!-- Footer End -->

            <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
        </div>

        <!-- JavaScript Libraries -->
        <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/common/js.php')?>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
        <script>
            var dynamicHeading = "Livelihood & Community Dev't.";
            document.getElementById("dynamic-heading").innerHTML = dynamicHeading;
        </script>
    </body>
</html>
